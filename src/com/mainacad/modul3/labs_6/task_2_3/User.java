package com.mainacad.modul3.labs_6.task_2_3;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    List<PermissionAction> permissions = new ArrayList<>();

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{ " +
                "name='" + name +
                " }";
    }

    public List<PermissionAction> getPermissions() {
        return permissions;
    }

    public void setPermissions(PermissionAction permission) {
        this.permissions.add(permission);
    }
}
