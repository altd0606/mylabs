package com.mainacad.modul3.labs_6.task_2_3;

import java.io.*;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Action {
    private File file;

    public Action() {
        this.file = new File("src/files/annotationFile.txt");
    }

    private String readFile(){
        String line = null;
        String text = "";
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(this.file));
            while ((line = fileReader.readLine())!=null){
                text += line+"\n";
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return text;
    }

    @MyPermission(PermissionAction.USER_READ)
    public String read(User user){
        String text = null;
        try {
            Method met = this.getClass().getMethod("read", User.class);
            MyPermission permissionAction = met.getAnnotation(MyPermission.class);
            //PermissionAction pr = permissionAction.value();
            if (user!=null && (user.getPermissions().contains(permissionAction.value()))){
                text = readFile();
                System.out.println(user.toString() + "read is successful");
            }else {
                System.out.println(user.toString() + " operation of read is permitted");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return text;
    }


    private void writeFile(){
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(file.length());
            Scanner scanner = new Scanner(System.in);
            System.out.println("Input data :");
            String inputInfo = scanner.nextLine();
            randomAccessFile.seek(randomAccessFile.length());
            randomAccessFile.writeBytes("\n");
            randomAccessFile.write(inputInfo.getBytes());

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @MyPermission(PermissionAction.USER_CHANGE)
    public void write(User user){
        try {
            Method met = this.getClass().getMethod("write", User.class);
            MyPermission permissionAction = met.getAnnotation(MyPermission.class);
            //PermissionAction pr = permissionAction.value();

            if (user != null && (user.getPermissions().contains(permissionAction.value()))){
                writeFile();
                System.out.println(user.toString() + "write is successful");
            }else {
                System.out.println(user.toString() + "operation of write is permitted");
            }
        }catch (NoSuchMethodException e){
            e.printStackTrace();
        }
    }
}
