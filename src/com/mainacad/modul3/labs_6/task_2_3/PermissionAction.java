package com.mainacad.modul3.labs_6.task_2_3;

public enum PermissionAction {
    USER_READ, USER_CHANGE
}
