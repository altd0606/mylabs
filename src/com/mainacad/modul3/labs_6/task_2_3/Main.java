package com.mainacad.modul3.labs_6.task_2_3;

public class Main {
    public static void main(String[] args) {
        User user = new User("FirstUser");
        User user1 = new User("SecondUser");
        user.setPermissions(PermissionAction.USER_CHANGE);
        user.setPermissions(PermissionAction.USER_READ);

        user1.setPermissions(PermissionAction.USER_READ);

        Action action = new Action();
        action.read(user1);
        action.read(user);
        action.write(user1);
        action.write(user);
    }
}
