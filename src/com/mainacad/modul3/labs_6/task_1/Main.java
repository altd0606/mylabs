package com.mainacad.modul3.labs_6.task_1;

public class Main {
    @Deprecated
    private static int findMax(int... array){
        int maxValue = Integer.MIN_VALUE;
        for (int arrValue:array) {
            maxValue = Math.max(arrValue,maxValue);
            //maxValue = maxValue < arrValue ? arrValue : maxValue;
        }
        return maxValue;
    }

    @SafeVarargs
    private static  <T> T findMax(T...array){
        T rez = null;
        for (T arrValue:array) {
            if (rez.equals(arrValue)){
                rez = arrValue;
            }
        }
        return rez;
    }

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        //int maxValue = findMax(1,2,3);
        //System.out.println(findMax(1,2,3));
        Integer i = 1;
        Integer i2 = 2;
        //Integer rez = findMax(i,i2);
    }
}
