package com.mainacad.modul3.labs_9.task_1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class CalculateProxy implements InvocationHandler  {
    private final Object objCalc;

    private CalculateProxy(Object objCalc) {
        this.objCalc = objCalc;
    }

    public static Object newInstance(Object objCalc){
        return Proxy.newProxyInstance(objCalc.getClass().getClassLoader(), objCalc.getClass().getInterfaces(), new CalculateProxy(objCalc));
    }

/*    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("Method name " + method.getName() + " (" +
                args[0] + ", " + args[1] + ") -> " +
                method.invoke(objCalc, args));
        return method.invoke(objCalc, args);
    }*/

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append(method.getName());
        stringBuffer.append(" (");

        for (int i = 0; args!=null && i < args.length; i++){
            if (i != 0){
                stringBuffer.append(", ");
            }
            stringBuffer.append(args[i]);
        }

        stringBuffer.append(")");

        Object result = method.invoke(objCalc, args);

        if (result != null){
            stringBuffer.append(" -> ");
            stringBuffer.append(result);
        }
        System.out.println(stringBuffer);
        return result;
    }


}
