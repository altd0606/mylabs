package com.mainacad.modul3.labs_9.task_1;

public interface Calculate {
    double multiplication(double x, double y);
    double division(double x, double y);
}
