package com.mainacad.modul3.labs_9.task_1;

public class Main {
    public static void main(String[] args) {
        //CalculateImpl calculate = new CalculateImpl();
        //CalculateProxy calculateProxy = (CalculateProxy) CalculateProxy.newInstance(calculate);
        Calculate calculateProxy = (Calculate) CalculateProxy.newInstance(new CalculateImpl());
        calculateProxy.division(10,2);
        calculateProxy.multiplication(10,2);

        /*Calculate calculateInterface = (Calculate) Proxy.newProxyInstance(Calculate.class.getClassLoader(),
                new Class[]{Calculate.class},
                calculateProxy);*/

        //calculateInterface.division(10,2);
    }
}
