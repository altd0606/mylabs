package com.mainacad.modul3.labs_9.referenceAnswer;

public interface Calculate {
    double multiplication(double x, double y);
    double division(double x, double y);
}
