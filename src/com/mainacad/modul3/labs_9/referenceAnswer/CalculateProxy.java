package com.mainacad.modul3.labs_9.referenceAnswer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;

public class CalculateProxy implements InvocationHandler {
    private Class[] interfaces;
    private Object[] delegates;

    private CalculateProxy(Class[] interfaces, Object[] delegates) {
        this.interfaces = interfaces;
        this.delegates = delegates;
    }

    public static Object newInstance(Class obj) {
        Object[] delegators = new Object[] {new CalculateImpl(), new CalculateBitwiseImpl()};
        List<Class<?>> listInterfaces = Arrays.asList(Calculate.class, CalculateBitwise.class);
        if ( listInterfaces.contains(obj)) {
            Class[] proxyInterfaces = listInterfaces.toArray(new Class[listInterfaces.size()]);
            return Proxy.newProxyInstance(
                    obj.getClassLoader(),
                    proxyInterfaces,
                    new CalculateProxy(proxyInterfaces, delegators));
        }
        System.out.println("Can not create object!");
        return null;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Class declaringClass = method.getDeclaringClass();
        for (int i=0; i<interfaces.length; i++) {
            if (declaringClass.isAssignableFrom(interfaces[i])) {
                return method.invoke(delegates[i], args);
            }
        }
        System.out.println("Method cannot be dispatched");
        return null;
    }

}