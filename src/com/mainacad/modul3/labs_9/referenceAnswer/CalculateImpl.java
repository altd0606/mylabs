package com.mainacad.modul3.labs_9.referenceAnswer;

public class CalculateImpl implements Calculate {
    @Override
    public double multiplication(double x, double y) {
        return (x*y);
    }

    @Override
    public double division(double x, double y) {
        return (x/y);
    }
}
