package com.mainacad.modul3.labs_9.referenceAnswer;

public interface CalculateBitwise {
    int andBitwise(int x, int y);
    int orBitwise(int x, int y);
}
