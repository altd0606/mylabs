package com.mainacad.modul3.labs_9.referenceAnswer;

public class CalculateBitwiseImpl implements CalculateBitwise {

    @Override
    public int andBitwise(int x, int y) {
        return x & y;
    }

    @Override
    public int orBitwise(int x, int y) {
        return x | y;
    }
}
