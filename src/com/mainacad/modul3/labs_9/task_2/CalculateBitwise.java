package com.mainacad.modul3.labs_9.task_2;

public interface CalculateBitwise {
    int andBitwise(int x, int y);
    int orBitwise(int x, int y);
}
