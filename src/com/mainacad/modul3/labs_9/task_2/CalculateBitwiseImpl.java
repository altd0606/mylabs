package com.mainacad.modul3.labs_9.task_2;

public class CalculateBitwiseImpl implements CalculateBitwise {

    @Override
    public int andBitwise(int x, int y) {
        return x & y;
    }

    @Override
    public int orBitwise(int x, int y) {
        return x | y;
    }
}
