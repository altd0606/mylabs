package com.mainacad.modul3.labs_9.task_2;

public class Main {
    public static void main(String[] args) {
        //CalculateImpl calculate = new CalculateImpl();
        //CalculateProxy calculateProxy = (CalculateProxy) CalculateProxy.newInstance(calculate);
        Calculate calculateProxy = (Calculate) CalculateProxy.newInstance(new CalculateImpl());
        calculateProxy.division(10,2);
        calculateProxy.multiplication(10,2);

        CalculateBitwise calculateProxy1 = (CalculateBitwise) CalculateProxy.newInstance(new CalculateBitwiseImpl());
        calculateProxy1.andBitwise(10,2);
        calculateProxy1.orBitwise(10,2);

        /*Calculate calculateInterface = (Calculate) Proxy.newProxyInstance(Calculate.class.getClassLoader(),
                new Class[]{Calculate.class},
                calculateProxy);*/

        //calculateInterface.division(10,2);
    }
}
