package com.mainacad.modul3.labs_9.task_2;

public class CalculateImpl implements Calculate{
    @Override
    public double multiplication(double x, double y) {
        return (x*y);
    }

    @Override
    public double division(double x, double y) {
        return (x/y);
    }
}
