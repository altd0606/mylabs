package com.mainacad.modul3.labs_8.task_3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer implements Runnable {
    private ServerThread serverThread;

    private void handlingQuery(){
        try(ServerSocket listener = new ServerSocket(9898)) {
            while (true){
                System.out.println("Server wait connection");
                Socket client = listener.accept();
                System.out.println("Server connected");
                ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());

                ServerThread serverThread = new ServerThread();
                serverThread.setObjectInputStream(objectInputStream);
                serverThread.setClient(client);
                serverThread.run();
                objectInputStream.close();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        handlingQuery();
    }
}
