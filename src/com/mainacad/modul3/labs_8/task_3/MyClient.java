package com.mainacad.modul3.labs_8.task_3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class MyClient implements Runnable {
    private void excangeData(){
        Socket socket = null;
        try {
            for (int i = 0; i < 10; i++){
                socket = new Socket("localhost",9898);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject("PING");
                objectOutputStream.flush();
                Thread.sleep(1000);
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                System.out.println(objectInputStream.readObject() + " " + i);
                objectInputStream.close();
                objectOutputStream.close();
            }
        }catch (IOException | InterruptedException | ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run(){
        excangeData();
    }
}
