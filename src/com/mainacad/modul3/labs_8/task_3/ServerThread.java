package com.mainacad.modul3.labs_8.task_3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread implements Runnable {
    private ObjectInputStream objectInputStream;
    private Socket client;

    @Override
    public void run(){
        try {
            String str = (String) objectInputStream.readObject();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(client.getOutputStream());
            objectOutputStream.writeObject(Thread.currentThread().getName() + " " + str + "-PONG");
            objectOutputStream.flush();
        }catch (ClassNotFoundException | IOException e){
            e.printStackTrace();
        }finally {
            try {
                client.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    public void setObjectInputStream(ObjectInputStream objectInputStream) {
        this.objectInputStream = objectInputStream;
    }

    public void setClient(Socket client) {
        this.client = client;
    }
}
