package com.mainacad.modul3.labs_8.task_4;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class MySender {

    public static void main(String[] args) {
/*        File file = new File("src/files/datagram.txt");
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write("someText");
            fileWriter.flush();
        }catch (IOException e){
            e.printStackTrace();
        }*/

        try {
            DatagramSocket serverSocket = new DatagramSocket();

            while (true){
                try {
                    InetAddress inetAddress = InetAddress.getByName("localhost");
                    DatagramPacket packet = new DatagramPacket(
                            new FileInputStream(new File("src/files/datagram.txt")).readAllBytes(),
                            new FileInputStream(new File("src/files/datagram.txt")).readAllBytes().length,
                            inetAddress,9999);

                    serverSocket.send(packet);
                }catch (IOException e){
                    e.printStackTrace();
                }
                System.out.println("message receive");
            }
        }catch (SocketException e) {
            e.printStackTrace();
        }

    }
}
