package com.mainacad.modul3.labs_8.task_4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;

public class MyRecipient {
    public static void main(String[] args) {
        byte[] buffer = new byte[256];
        try {
            DatagramSocket socket = new DatagramSocket(9999);
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            System.out.println(new String(packet.getData()));
           /* FileInputStream fileInputStream = new FileInputStream(new File(new String(packet.getData())));
            System.out.println(fileInputStream.read());*/

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
