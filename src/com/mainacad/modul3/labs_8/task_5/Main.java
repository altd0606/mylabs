package com.mainacad.modul3.labs_8.task_5;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://www.brainacad.com");
            URLConnection urlConnection = url.openConnection();
            System.out.println(urlConnection.getInputStream().read());
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
