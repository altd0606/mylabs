package com.mainacad.modul3.labs_8.task_1_2;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class MyClient implements Runnable{
    private Student student;

    public MyClient(Student student) {
        this.student = student;
    }

    public void run(){
        try(Socket socket = new Socket("localhost", 8989)) {

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());

            objectOutputStream.writeObject(student);
            objectOutputStream.flush();

            System.out.println("Server response" + objectInputStream.readObject());

            objectInputStream.close();
            objectOutputStream.close();
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
