package com.mainacad.modul3.labs_8.task_1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MyServer implements Runnable{
    private List<Student> studentList = new ArrayList<>();

    public void setStudentList() {
        studentList.add(new Student("A","jAvA"));
        studentList.add(new Student("b","jAvAA"));
        studentList.add(new Student("c","jAvA"));
        studentList.add(new Student("d","jAvA"));
    }

    public void run(){
        setStudentList();
        try(ServerSocket listener = new ServerSocket(8989)) {
            while (true){
                System.out.println("Server wait");
                Socket socket = listener.accept();
                System.out.println("Server connected");


                ThreadClient threadClient = new ThreadClient();
                threadClient.setSocket(socket);
                threadClient.setStudents(studentList);
                threadClient.run();
            }


/*            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

            Student student = (Student) objectInputStream.readObject();
            System.out.println(student);

            objectOutputStream.writeObject(student.getName() + " Received");
            objectOutputStream.flush();

            objectInputStream.close();
            objectOutputStream.close();*/

            //System.out.println("Server dictonnected");
        }catch (IOException /*| ClassNotFoundException*/ e){
            e.printStackTrace();
        }

    }
}
