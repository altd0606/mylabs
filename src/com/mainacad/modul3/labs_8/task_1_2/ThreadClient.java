package com.mainacad.modul3.labs_8.task_1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class ThreadClient implements Runnable {
    List<Student> students;
    Socket socket;

    @Override
    public void run() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

            Student student = (Student) objectInputStream.readObject();
            //System.out.println(student);

            if (students.contains(student)){
                //System.out.println(student);

                objectOutputStream.writeObject(student.getName() + " Received");
                objectOutputStream.flush();
            } else {
                objectOutputStream.writeObject(student.toString() + "dont have acces");
                objectOutputStream.flush();
            }

            objectInputStream.close();
            objectOutputStream.close();

        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
