package com.mainacad.modul3.labs_8.task_1_2;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("someName", "someCourse");
        Student student1 = new Student("A", "someCourse");

        new Thread(new MyServer()).start();
        new Thread(new MyClient(student)).start();
        new Thread(new MyClient(student1)).start();
    }
}
