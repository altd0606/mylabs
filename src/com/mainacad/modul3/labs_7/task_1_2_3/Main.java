package com.mainacad.modul3.labs_7.task_1_2_3;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    private static List<Integer> sumEvent(Integer[] array, Predicate<Integer> predicate){
        List<Integer> rezult = new ArrayList();
        for (int element : array){
            if (predicate.test(element)){
                rezult.add(element);
            }
        }
        return rezult;
    }

    private static void printJStr(List<String> listOfStrings, Predicate<String> predicate){
        for (String element : listOfStrings) {
            if (predicate.test(element)){
                System.out.println(element);
            }
        }
    }

    private static void updateList(List<String> listOfStrings, MyConverter myConverter){
        for (int i = 0; i < listOfStrings.size(); i++){
            listOfStrings.set(i,myConverter.convertStr(listOfStrings.get(i)));
        }
    }

    public static void main(String[] args) {
        Integer[] array;

        array = new Random().ints(20,0,100)
                .boxed()
                .toArray(Integer[]::new);

        /*Stream
                .iterate(10,n -> (int)(n*Math.random()*n))
                .limit(20)
                .collect(Collectors.toList())
                .toArray(array);*/

        Arrays.stream(array).forEach(System.out::println);
        System.out.println("-------------------------");

        Arrays.stream(array)
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList())
                .toArray(array);

        Arrays.stream(array).forEach(System.out::println);

        List<String> stringList = new ArrayList<>();
        stringList.add("ABCD");
        stringList.add("BCDE");
        stringList.add("CDEF");
        stringList.add("DEFG");
        stringList.add("A");

        stringList = stringList.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());

        stringList.forEach(System.out::println);

        System.out.println("-----------------------------");
        sumEvent(array,x -> x < 10).stream().forEach(System.out::println);

        System.out.println("-----------------------------");
        printJStr(stringList, x -> x.charAt(0)=='A');

        System.out.println("-----------------------------");
        updateList(stringList,String::toLowerCase); //x -> x.toLowerCase
        stringList.forEach(System.out::println);
    }
}
