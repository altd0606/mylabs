package com.mainacad.modul3.labs_7.task_1_2_3;

public interface MyConverter {
    String convertStr(String str);
    static boolean isNull(String str){
        return str==null;
    }
}
