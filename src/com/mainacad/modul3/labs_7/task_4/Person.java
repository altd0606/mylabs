package com.mainacad.modul3.labs_7.task_4;


import java.util.Random;

public class Person {
    static String[] genders = {"male", "female"};
    private String name;
    private int age;
    private String gender;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Person(String name) {
        this.name = name;
        int randomAge = new Random().nextInt(80);
        this.age = randomAge;
        int gen = new Random().nextInt(2);
        this.gender = genders[gen];
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}
