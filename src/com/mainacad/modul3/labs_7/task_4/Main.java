package com.mainacad.modul3.labs_7.task_4;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<Integer> listofNum = Stream.iterate(10,n -> n+10).limit(10).map(x -> x/2).collect(Collectors.toList());

        listofNum.stream().filter(x -> (x > 10) && (x < 30)).forEach(System.out::println);
        System.out.println("----------------");
        listofNum.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);
        System.out.println("-----------------");
        List<Person> people = Stream.generate(() -> new Person("name")).limit(50).collect(Collectors.toList());

        people.stream().filter(x -> (x.getAge() >= 18) && (x.getAge()<=27) && (x.getGender().equals("male"))).forEach(System.out::println);
        double average = people.stream().filter(x -> x.getGender().equals("female")).mapToInt(Person::getAge).average().getAsDouble();

        System.out.println("Female average age : " + average);

    }
}
