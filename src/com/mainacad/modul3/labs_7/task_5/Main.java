package com.mainacad.modul3.labs_7.task_5;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();
        System.out.println(date);
        LocalDate birth = LocalDate.of(2000,6,6);
        System.out.println(birth);

        Period period = Period.between(birth,date);
        System.out.println(period.getYears());
        System.out.println(birth.getDayOfWeek());
        System.out.println(birth.plusYears(20).getDayOfWeek());
        System.out.println(birth.until(date));

        if (birth.until(date).getDays()>0){
            System.out.println("Birthday alredy was");
        }
        else {
            System.out.println("You will selebrrabe later");
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH-mm-ss");

        Clock clock = Clock.system(ZoneId.of("Europe/Paris"));
        Clock clock1 = Clock.system(ZoneId.of("Europe/Berlin"));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(clock.instant(), ZoneId.of("UTC"));

        LocalDateTime localDateTime1 = LocalDateTime.ofInstant(clock1.instant(),ZoneId.of("UTC"));
        System.out.println(localDateTime.format(dateTimeFormatter));
        System.out.println(localDateTime1);

    }
}
