package com.mainacad.modul3.labs_2.task_5;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        User[] listOfUsers = new User[10];

        for (int i = 0; i < listOfUsers.length; i++){
            listOfUsers[i] = new User("FirstName " + i,"LastName " + i,(int)(Math.random()*90));
        }

        RandomAccessFile randomAccessFile = null;
        ObjectOutputStream objectOutputStream = null;
        FileOutputStream fileOutputStream = null;

        try {
            //randomAccessFile = new RandomAccessFile("src/files/userExternalizable.ser","rw");
            fileOutputStream = new FileOutputStream("src/files/userExternalizable.ser");
            //fileOutputStream = new FileOutputStream(randomAccessFile);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            for (User userToSerealize : listOfUsers){
                //randomAccessFile.seek(randomAccessFile.length());
                objectOutputStream.writeObject(userToSerealize);
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                //randomAccessFile.close();
                objectOutputStream.close();
                fileOutputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        User userForDeserialization;


        ObjectInputStream objectInputStream = null;
        BufferedInputStream bufferedInputStream = null;
        try{
            bufferedInputStream = new BufferedInputStream(new FileInputStream("src/files/userExternalizable.ser"));
            objectInputStream = new ObjectInputStream(bufferedInputStream);
            while(bufferedInputStream.available()>0){
                userForDeserialization = (User)objectInputStream.readObject();
                System.out.println(userForDeserialization);
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            try{
                bufferedInputStream.close();
                objectInputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }
}
