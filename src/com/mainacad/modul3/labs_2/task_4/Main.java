package com.mainacad.modul3.labs_2.task_4;

import com.mainacad.modul3.labs_2.task_1_2_3.Employee;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        User user = new User("List","Last",23);

        RandomAccessFile randomAccessFile = null;
        ObjectOutputStream objectOutputStream = null;
        FileOutputStream fileOutputStream = null;

        try {
            randomAccessFile = new RandomAccessFile("src/files/user.ser","rw");
            fileOutputStream = new FileOutputStream(randomAccessFile.getFD());
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            randomAccessFile.seek(randomAccessFile.length());
            objectOutputStream.writeObject(user);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                //randomAccessFile.close();
                objectOutputStream.close();
                fileOutputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        User userForDeserialization = null;

        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;

        try{
            fileInputStream = new FileInputStream("src/files/user.ser");
            objectInputStream = new ObjectInputStream(fileInputStream);
            userForDeserialization = (User) objectInputStream.readObject();
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            try{
                fileInputStream.close();
                objectInputStream.close();
                randomAccessFile.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        System.out.println(userForDeserialization.toString());
    }
}
