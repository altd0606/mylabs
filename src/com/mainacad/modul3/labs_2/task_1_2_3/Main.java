package com.mainacad.modul3.labs_2.task_1_2_3;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.setAddress("Some address");
        employee.setName("Some Name");
        employee.setNumber(1);
        employee.setSSN(1);

        System.out.println(employee);

        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream("src/files/employee.ser");
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(employee);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                fileOutputStream.close();
                objectOutputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        Employee employeeForDeserialization = null;

        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;

        try{
            fileInputStream = new FileInputStream("src/files/employee.ser");
            objectInputStream = new ObjectInputStream(fileInputStream);
            employeeForDeserialization = (Employee) objectInputStream.readObject();
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            try{
                fileInputStream.close();
                objectInputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        System.out.println(employeeForDeserialization.toString());



    }
}
