package com.mainacad.modul3.labs_1.task_3;

import java.io.*;

public class MyFileCopy {
    public static void main(String[] args) {
/*        InputStream inputStream = null;
        OutputStream outputStream= null;

        try {
            //FileInputStream fileInputStream = new FileInputStream("src/file.txt");
            //FileOutputStream fileOutputStream = new FileOutputStream("src/fileOutput.txt");
            inputStream = new FileInputStream("src/files/file.txt");
            outputStream = new FileOutputStream("src/files/fileOutput.txt");
            int readerForByte = 0;

            while ((readerForByte = inputStream.read()) != -1){
                outputStream.write(readerForByte);
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        try(InputStream inputStream = new FileInputStream("src/files/file.txt")){
            try(OutputStream outputStream = new FileOutputStream("src/files/fileOutput.txt")){
                int readerForByte = 0;

                while ((readerForByte = inputStream.read()) != -1){
                    outputStream.write(readerForByte);
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
