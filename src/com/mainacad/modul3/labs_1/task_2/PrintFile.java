package com.mainacad.modul3.labs_1.task_2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PrintFile {
    public static void main(String[] args) {
/*        BufferedReader bufferedReader = null;
        try {
            //FileReader fileReader = new FileReader("src/ file.txt");
            bufferedReader = new BufferedReader(new FileReader("src/files/file.txt"));
            String readeredStr;
            while((readeredStr = bufferedReader.readLine()) != null){
                System.out.println(readeredStr);
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                bufferedReader.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }*/

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/files/file.txt"))){

            String readeredStr;
            while((readeredStr = bufferedReader.readLine()) != null){
                System.out.println(readeredStr);
            }

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
