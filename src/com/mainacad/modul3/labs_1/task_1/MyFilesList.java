package com.mainacad.modul3.labs_1.task_1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

public class MyFilesList {
    public static void main(String[] args) {

        File file = new File("src/files/args.txt");

        try(FileWriter out = new FileWriter("src/files/args.txt")){
            if (args.length!=0){
                out.write(args[0]);
            }else{
                out.write(file.getAbsolutePath());
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
