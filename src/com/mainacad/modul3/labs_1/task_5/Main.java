package com.mainacad.modul3.labs_1.task_5;

public class Main {
    public static void main(String[] args) {
        AccountingUser accountingUser = new AccountingUser();
        accountingUser.testUser("Name1");
        accountingUser.testUser("name2");
        accountingUser.printFile();
        accountingUser.finalize();
    }
}
