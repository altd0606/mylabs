package com.mainacad.modul3.labs_1.task_5;

import java.io.IOException;
import java.io.RandomAccessFile;


public class AccountingUser {
    private static RandomAccessFile file = null;
    private String PATH = "src/files/users.txt";

    AccountingUser(){
        try{
            file = new RandomAccessFile(PATH,"rw");
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void testUser(String nameUser){
        try{
            file.seek(file.length());
            file.write((nameUser+"\n").getBytes());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void printFile(){
        try{
            StringBuffer strToPrint = new StringBuffer();
            int currentByte;
            file.seek(0);
            while((currentByte=file.read())!=-1){
                System.out.print((char)(currentByte));
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void finalize(){
        try {
            file.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


}
