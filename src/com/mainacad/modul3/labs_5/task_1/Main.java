package com.mainacad.modul3.labs_5.task_1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        Class<?> cls = myClass.getClass();
        System.out.println(cls.getName());

        //Class<?> cls = MyClass.class;
        int mods = cls.getModifiers();
        System.out.println("Modifiers:");
        if (Modifier.isPrivate(mods)){
            System.out.println("private");
        }
        if(Modifier.isPublic(mods)){
            System.out.println("public");
        }
        if (Modifier.isFinal(mods)){
            System.out.println("final");
        }

        System.out.println("Public Fields:");
        Field[] fields = cls.getFields();
        for (Field field:fields) {
            Class<?> fType = field.getType();
            System.out.println("Name " + field.getName());
            System.out.println("Type " + fType.getName());
        }
        System.out.println("All fields:");
        fields = cls.getDeclaredFields();
        for (Field field:fields) {
            Class<?> fType = field.getType();
            System.out.println("Name : " + field.getName());
            System.out.println("Type : " + fType.getName());
        }


        System.out.println("Methods :");
        Method[] methods = cls.getDeclaredMethods();
        for (Method method:methods) {
            Class<?>[] paramTypes = method.getParameterTypes();
            System.out.println("Method name " + method.getName());
            System.out.println("Return type  " + method.getReturnType().getName());
            for (Class<?> paramType:paramTypes) {
                System.out.println("Parametrs " + paramType.getName());
            }
        }

        System.out.println("Constructors : ");

        Constructor<?>[] constructors = cls.getConstructors();
        for (Constructor constructor:constructors) {
            Class<?>[] paramTypes = constructor.getParameterTypes();
            System.out.println("Constructor - ");
            for (Class<?> paramType:paramTypes) {
                System.out.println(paramType.getName());
            }
        }

    }
}

final class MyClass{
    private int a = 10;
    public int b = 9;
    protected int c = 8;
    private static int d = 5;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getD() {
        return d;
    }

    public MyClass(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public MyClass(int a) {
        this.a = a;
    }

    public MyClass(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public MyClass(){}
}
