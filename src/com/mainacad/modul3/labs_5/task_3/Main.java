package com.mainacad.modul3.labs_5.task_3;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        Class<?> cls = MyClass.class;

        Constructor<?> cons = cls.getDeclaredConstructor(int.class);
        MyClass myClass = (MyClass) cons.newInstance(15);
        Method method = cls.getMethod("setA", int.class);
        method.invoke(myClass,33);

        Field[] fields = cls.getDeclaredFields();

        for (Field field: fields) {
            System.out.println("Name : " + field.getName());
            System.out.println("Type: " + field.getType());
            if (!field.isAccessible()){
                field.setAccessible(true);
            }
            System.out.println(field.get(myClass));
        }
    }
}
