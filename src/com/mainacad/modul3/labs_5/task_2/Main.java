package com.mainacad.modul3.labs_5.task_2;

import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        String mystr = "abcd";
        Field field = mystr.getClass().getDeclaredField("value");
        field.setAccessible(true);
        byte[] newStr = {'z','x','c','v'};
        System.out.println("mystr1 " + mystr);
        field.set(mystr,newStr);

        System.out.println("mystr2 " + mystr);
        System.out.println("abcd");

    }
}
