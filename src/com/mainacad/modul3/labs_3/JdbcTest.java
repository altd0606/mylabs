package com.mainacad.modul3.labs_3;

import java.sql.*;

public class JdbcTest {
    public static void main(String[] args) {
        final String db_url="jdbc:mysql://localhost/test?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        //final String db_url="jdbc:mysql://localhost/test";
        final String db_user="test";
        final String db_password="test";
        final String db_driver="com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(db_driver); // initialize driver
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try(Connection conn = DriverManager.getConnection(db_url, db_user, db_password)) {
            Statement st = conn.createStatement();
            st.execute("select * from test"); // may return more than one recordset
            st.executeQuery("select * from test"); // return only single recordset
            ResultSet rs = st.getResultSet();
            while (rs.next()) {
                System.out.println(rs.getString(2));
            }
            System.out.println("Prepared Statement");
            PreparedStatement ps = conn.prepareStatement("select * from test where id = ?");
            ps.setInt(1,2);
            ps.executeQuery();
            rs = ps.getResultSet();
            while(rs.next()) {
                System.out.println(rs.getString(2));
            }
            System.out.println("Meta Data");
            ResultSetMetaData rsm = ps.getMetaData();
            for (int i = 1; i <= rsm.getColumnCount(); i++) {
                System.out.print(rsm.getColumnName(i) + "\t\t");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

