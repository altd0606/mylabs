package com.mainacad.modul1;

public class ArrayOutput {
    public static void arrayOut(int[][] arrayToOutput){
        for (int i = 0; i<arrayToOutput.length;i++){
            for (int j = 0; j<arrayToOutput.length;j++){
                System.out.print(arrayToOutput[i][j] + " ");
            }
            System.out.println();
        }
    }
}
