package com.mainacad.modul1.labs_5;

import java.util.Scanner;

public class Lab7 {
    public static void main(String[] args) {
        long limitationNumber = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите последнее проверяемое число");
        limitationNumber = scanner.nextLong();

        for (long i = 1; i<=limitationNumber; i++){
            int sum = 0;
            for (long j = 1; j<i; j++){
                if(i%j==0){
                    sum+=j;
                }
            }
            if (sum==i){
                System.out.println(i);
            }
        }

    }
}
