package com.mainacad.modul1.labs_5;

public class Lab4 {
    public static void main(String[] args) {
        int countForBreak = 0;
        for (int i = 1; i<300; i++){
            if(i%3==0 || i%4==0){
                System.out.println(i);
                countForBreak++;
            }
            if (countForBreak==10){
                break;
            }
        }
    }
}
