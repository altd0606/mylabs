package com.mainacad.modul1.labs_6;

import java.util.Arrays;

public class Lab6 {
    public static void main(String[] args) {
        double[] averageTemperature = {-0.7,2.1,7.5,16,21.3,21,20.1,16.3,10.7,4.1,-1.1,-4.2};

        Arrays.sort(averageTemperature);

        System.out.println(Arrays.toString(averageTemperature));
    }

}
