package com.mainacad.modul1.labs_6;

import com.mainacad.modul1.ArrayOutput;

public class Lab3 {
    public static void main(String[] args) {
        int[][] squareArr = new int[4][4];
        int valueInTable;
        int startValue = 1;

        for (int i = 0; i<squareArr.length;i++){
            valueInTable = startValue;
            for (int j = 0; j<squareArr.length;j++){
                squareArr[i][j] = valueInTable;
                valueInTable+=4;
            }
            startValue++;
        }


        ArrayOutput.arrayOut(squareArr);

        int arr[][] = new int[4][4];
        int inputNum = 1;
        for (int i = 0; i<arr.length; i++){
            for (int j = 0;j<arr.length;j++){
                arr[j][i] = inputNum++;
            }
        }
        System.out.println();

        ArrayOutput.arrayOut(arr);


    }
}
