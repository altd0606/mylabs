package com.mainacad.modul1.labs_6;

import java.util.Arrays;
import java.util.Random;

public class Lab4 {
    public static void main(String[] args) {
        Random random  = new Random();
        int[] arr = new int[30];
        for (int i = 0; i<arr.length;i++){
            int randomNum = random.nextInt(50);
            arr[i] = randomNum;
        }

        Arrays.sort(arr);
        int searchNumber = Arrays.binarySearch(arr,13);
        System.out.println(searchNumber);
    }
}
