package com.mainacad.modul1.labs_6;

import java.util.Arrays;

public class Lab1 {
    public static void main(String[] args) {
        int[] arr = new int[15];
        int value = 2;

        for (int i =0;i<15;i++){
            arr[i] = value;
            value+=2;
        }

        /*for (int i= 0;i<arr.length;i++){
            System.out.println(arr[i]);
        }*/

        System.out.println(Arrays.toString(arr));
    }
}
