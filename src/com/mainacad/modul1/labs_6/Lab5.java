package com.mainacad.modul1.labs_6;

import com.mainacad.modul1.ArrayOutput;

import java.util.Random;

public class Lab5 {
    public static void main(String[] args) {
        int[][] matrixArr = new int[4][4];

        Random random =  new Random();

        for (int i = 0; i<matrixArr.length;i++){
            for (int j = 0; j<matrixArr.length;j++){
                int randomNum = random.nextInt(10);
                matrixArr[i][j] = randomNum;
            }
        }


        ArrayOutput.arrayOut(matrixArr);
        System.out.println();


        for (int i = 0; i<matrixArr.length;i++){

            for (int j = 0; j<i;j++){
                int tmp = matrixArr[i][j];
                matrixArr[i][j] = matrixArr[j][i];
                matrixArr[j][i] = tmp;
            }
        }

        ArrayOutput.arrayOut(matrixArr);

    }
}
