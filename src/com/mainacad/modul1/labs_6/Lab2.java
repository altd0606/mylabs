package com.mainacad.modul1.labs_6;

import java.util.Arrays;

public class Lab2 {
    public static void main(String[] args) {
        int[] m =new int[]{10,21,5,22,9,29,25,22,11,14,8,14};
        int maxSearch = 0;
        int minSearch = 100;
        int average = 0;
        int index = 0;
        boolean loopFlag = true;
        boolean flag = false;


        while (loopFlag){

            if (m.length-1==index){
                break;
            }

            if (m[index]<m[index+1]){
                int tmp = m[index];
                m[index] = m[index+1];
                m[index+1] = tmp;
                index++;
                flag = true;
            }
            else {
                index++;
            }

            if (flag){
                index = 0;
                flag = false;
            }

        }

        System.out.println(Arrays.toString(m));

        for (int i = 0; i<m.length;i++){
            if(m[i]>maxSearch){
                maxSearch = m[i];
            }
            if (m[i]<minSearch){
                minSearch = m[i];
            }
            average+=m[i];
        }

        System.out.println("Max : " + maxSearch);
        System.out.println("Min : " + minSearch);
        System.out.println("Average : " + average/m.length);


    }
}
