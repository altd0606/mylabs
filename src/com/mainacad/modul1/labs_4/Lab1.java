package com.mainacad.modul1.labs_4;

public class Lab1 {
    public static void main(String[] args){
        int a = 1;
        byte b = 127;
        short c = 11;
        long l = 12345678L;
        float f = 3.14F;
        double d = 666.666;
        char h = 'c';
        boolean flag = true;

        System.out.println(a + "\n" + b + "\n" + c + "\n" + l + "\n" + f + "\n" + d + "\n" + h + "\n" + flag);
    }
}
