package com.mainacad.modul1.labs_4;

public class Lab4 {
    public static void main(String[] args){
        int i = 0;
        int b = 0;
        System.out.println(b = i++ + i++); //1
        i = 0;
        b = 0;
        System.out.println(b = i++ + ++i); //2
        i = 0;
        b = 0;
        System.out.println(b = ++i + i++); //2
        i = 0;
        b = 0;
        System.out.println(b = i++ + i++ + i++); //3

        System.out.println("minus");

        i = 0;
        b = 0;
        System.out.println(b = i-- + i--); //-1
        i = 0;
        b = 0;
        System.out.println(b = i-- + --i); //-2
        i = 0;
        b = 0;
        System.out.println(b = --i + i--); //-2
        i = 0;
        b = 0;
        System.out.println(b = i-- + i-- + i-- + --i); //-7  0 + -1 + -2 + -4
    }
}
