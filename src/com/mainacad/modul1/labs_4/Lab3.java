package com.mainacad.modul1.labs_4;

public class Lab3 {
    public static void main(String[] args){
        boolean truthVariable = true;
        boolean lyingVarible = false;

        System.out.println("\t" + "OR  \t" + "AND \t" + "NOT \t" + "XOR");


        System.out.println("\t" + (truthVariable||truthVariable) + "\t" + (truthVariable&&truthVariable) + "\t" + (truthVariable!=truthVariable) + "\t" + (truthVariable^truthVariable) + "\t две переменные true");
        System.out.println("\t" + (truthVariable||lyingVarible) + "\t" + (truthVariable&&lyingVarible) + "\t" + (truthVariable!=lyingVarible) + "\t" + (truthVariable^lyingVarible) + "\t одна переменная true вторая false");
        System.out.println("\t" + (lyingVarible||lyingVarible) + "\t" + (lyingVarible&&lyingVarible) + "\t" + (lyingVarible!=lyingVarible) + "\t" + (lyingVarible^lyingVarible) + "\t две переменные false\n");

        System.out.println("\t" + (truthVariable|truthVariable) + "\t" + (truthVariable&truthVariable) + "\t две переменные true");
        System.out.println("\t" + (truthVariable|lyingVarible) + "\t" + (truthVariable&lyingVarible) + "\t одна переменная true вторая false");
        System.out.println("\t" + (lyingVarible|lyingVarible) + "\t" + (lyingVarible&lyingVarible)+ "\t две переменные false");
    }
}
