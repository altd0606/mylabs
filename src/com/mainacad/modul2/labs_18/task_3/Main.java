package com.mainacad.modul2.labs_18.task_3;

import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        Long num = 2430000000000L;
        Double dNum = 0.002;
        NumberFormat currencydefault = NumberFormat.getCurrencyInstance();
        System.out.println(currencydefault.format(num));

        Date date = new Date();
        System.out.println(date);
        Locale locale = Locale.getDefault();
        //Locale locale = new Locale("uk_UA");
        System.out.println(locale.getDisplayCountry());
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        System.out.println(numberFormat.format(num));
        System.out.println(numberFormat.format(dNum));
    }
}
