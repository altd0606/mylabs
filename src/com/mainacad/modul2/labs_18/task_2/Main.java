package com.mainacad.modul2.labs_18.task_2;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        BigDecimal bottomRamge = new BigDecimal("0.1");
        final BigDecimal upperRamge = new BigDecimal("1.1");
        final BigDecimal noMoney = new BigDecimal("0.0");

        for(BigDecimal x = bottomRamge; x.compareTo(upperRamge)<0;x = x.add(bottomRamge)){
            int numsOfNails = 0;
            BigDecimal money = new BigDecimal("1.0");
            //Currency currency = Currency.getInstance(Locale.US);
            NumberFormat numUs = NumberFormat.getCurrencyInstance(Locale.US);
            for (;money.compareTo(noMoney)>0;){
                money = money.subtract(x);
                if (money.compareTo(noMoney)<0){
                    money = money.add(x);
                    break;
                }
                numsOfNails++;
            }
            System.out.println("Nails : " + numsOfNails + " " + numUs.format(money) + " with price : " + numUs.format(x));
        }

    }
}
