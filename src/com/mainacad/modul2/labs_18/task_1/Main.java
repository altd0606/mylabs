package com.mainacad.modul2.labs_18.task_1;

import java.math.BigInteger;
import java.util.Random;

public class Main {
    public static BigInteger factorial(BigInteger n) {
        BigInteger result = new BigInteger("1");
        if (n.compareTo(BigInteger.valueOf(1))==0 || n.compareTo(BigInteger.valueOf(0))==0) {
            return result;
        }
        result = n.multiply(factorial(n.subtract(BigInteger.valueOf(1))));
        return result;
    }
    public static void main(String[] args) {
        Random random = new Random();
        int randomNum = random.nextInt((50-10)+1)+10;
        System.out.println(randomNum);
        BigInteger bigRandom = new BigInteger(String.valueOf(randomNum));
        BigInteger rezult = factorial(bigRandom);
        System.out.println(rezult);
    }
}
