package com.mainacad.modul2.labs_17.task_3_4;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[1000];
        for (int i = 0; i < arr.length; i++){
            arr[i] = random.nextInt(Integer.MAX_VALUE/10000);//Integer.MAX_VALUE
        }

        MySumCount mySumCount = new MySumCount(arr,0,arr.length/2);
        Thread thread = new Thread(mySumCount);
        thread.start();


        MySumCount mySumCount1 = new MySumCount(arr,arr.length/2,arr.length);
        Thread thread1 = new Thread(mySumCount1);
        thread1.start();

        try {
            thread.join();
            thread1.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(mySumCount.getRezultSum()+mySumCount1.getRezultSum());
        //join wait for first way

        MySumCount mySumCount3 = new MySumCount(arr,0,arr.length/2);
        mySumCount3.start();


        MySumCount mySumCount4 = new MySumCount(arr,arr.length/2,arr.length);
        mySumCount4.start();

        try {
            mySumCount3.join();
            mySumCount4.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(mySumCount.getRezultSum()+mySumCount1.getRezultSum());
        System.out.println(mySumCount3.getRezultSum()+mySumCount4.getRezultSum());
    }
}
