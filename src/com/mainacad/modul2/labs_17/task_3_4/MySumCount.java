package com.mainacad.modul2.labs_17.task_3_4;

public class MySumCount extends Thread implements Runnable{
    int startIndex;
    int endIndex;
    long rezultSum;
    int MySumCount[];

    public int[] getMySumCount() {
        return MySumCount;
    }

    public void setMySumCount(int[] mySumCount) {
        MySumCount = mySumCount;
    }

    public long getRezultSum(){
        return rezultSum;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    MySumCount(int[] inputArr, int startIndex, int endIndex){
        setMySumCount(inputArr);
        setStartIndex(startIndex);
        setEndIndex(endIndex);
    }

    @Override
    public void run(){
        for (int i = startIndex; i < endIndex; i++){
            System.out.println(Thread.currentThread().getName() + " " + MySumCount[i]);
            rezultSum+=MySumCount[i];
        }
    }
}
