package com.mainacad.modul2.labs_17.task_7_8;

import java.util.Random;

public class Transfer extends Thread{
    private Bank bank;
    private int from;
    private int max;
    public Transfer(Bank bank, int from, int max_amount) {
        this.bank = bank;
        this.from = from;
        max = max_amount;
    }

    @Override
    public void run(){
        while (true){
            bank.transfer(from,bank.randomAccount(from),(int) (Math.random()*1000));
        }
    }
}
