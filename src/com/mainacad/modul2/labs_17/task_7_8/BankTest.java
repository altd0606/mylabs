package com.mainacad.modul2.labs_17.task_7_8;

public class BankTest {
    public static final int N_ACCOUNTS = 5;
    public static final int INIT_BALANCE = 1000;

    public static void main(String[] args) {
        Bank bank = new Bank(N_ACCOUNTS,INIT_BALANCE);

        for (int i = 0; i < N_ACCOUNTS; i++){
            Transfer transfer = new Transfer(bank,i,(int)(Math.random()*INIT_BALANCE));
            transfer.start();
        }

    }
}
