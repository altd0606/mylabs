package com.mainacad.modul2.labs_17.task_10;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ConcurrentHashMap;

public class Mapthread extends Thread {
    Map<Integer, Date> map;
    ArrayList<Integer>keyStorage;

    Mapthread(Map<Integer, Date> map){
        this.map = map;
        keyStorage = new ArrayList<>();
    }

    public Date readFromMap(){
        return map.get(keyStorage.get((int)(Math.random()*(keyStorage.size()-1))));
    }

    public void wroteInMap(){
        int key = (int) (Math.random()*Integer.MAX_VALUE);
        keyStorage.add(key);
        map.put(key,new Date());
    }

    public void run(){
        wroteInMap();
        System.out.println(readFromMap() + " " + Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            return;
        }

    }





    public static void main(String[] args){
        Map<Integer, Date> map = new ConcurrentHashMap();
        Mapthread mapthread = new Mapthread(map);
        ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(4);

        for (int i = 0; i < 100; i++){
            executor.execute(mapthread);
           //mapthread.start();
        }
        executor.shutdown();
    }
}
