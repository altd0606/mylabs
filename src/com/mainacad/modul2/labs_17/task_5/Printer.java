package com.mainacad.modul2.labs_17.task_5;

public class Printer implements Runnable{
    Storage storage;
    Printer(Storage storage){
        this.storage = storage;
    }

    @Override
    public void run(){
        synchronized(storage){
            for (;storage.counterNum<=100;){
                System.out.println(storage.counterNum);

/*
                try{
                    Thread.sleep(1500);
                }catch (InterruptedException e){
                    return;
                }
*/

                storage.isReady = true;
                storage.notifyAll();
                if (storage.counterNum==100){
                    break;
                }
                try {
                    while (storage.isReady){
                        storage.wait();
                    }
                }catch (InterruptedException e){
                    return;
                }
            }
        }
        System.out.println("Print Thread end");
    }

}
