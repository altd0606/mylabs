package com.mainacad.modul2.labs_17.task_5;

public class Counter implements Runnable{
    Storage storage;
    Counter(Storage storage){
        this.storage = storage;
    }

    @Override
    public void run() {
        synchronized (storage){
            for (;storage.counterNum<100;){
                storage.counterNum++;
                storage.isReady = false;
                storage.notifyAll();
                try {
                    while (!storage.isReady){
                        storage.wait();
                    }
                }catch (InterruptedException e){
                    return;
                }
            }
        }
        System.out.println("count Thread end");
    }
}
