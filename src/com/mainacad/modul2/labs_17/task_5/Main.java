package com.mainacad.modul2.labs_17.task_5;

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        Counter counter = new Counter(storage);
        Printer printer = new Printer(storage);
        Thread thread = new Thread(counter);

        Thread thread1 = new Thread(printer);
        thread1.start();
        thread.start();


    }
}
