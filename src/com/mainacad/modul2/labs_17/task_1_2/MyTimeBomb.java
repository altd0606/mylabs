package com.mainacad.modul2.labs_17.task_1_2;

public class MyTimeBomb extends Thread implements Runnable{
    @Override
    public void run() {
        for (int i = 10; i >= 0; i--){
            System.out.print(Thread.currentThread().getName() + " ");
            if (i==0){
                System.out.println("Boom!");
                break;
            }
            System.out.println(i);
            try{
                sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }

    }
}
