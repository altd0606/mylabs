package com.mainacad.modul2.labs_17.task_6;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class DiningHall implements Runnable{
    static int pizzaNum;
    static int studentID = 1;

    public void makePizza() {
        pizzaNum++;
    }

    synchronized public void servePizza() {
        String result;
        if (pizzaNum > 0) {
            result = "Served "; pizzaNum--;
        }
        else result = "Starved ";
        System.out.println(result + studentID);
        studentID++;
    }

    @Override
    public void run(){
        servePizza();
    }

    public static void main(String[] args) {
        DiningHall d = new DiningHall();
        for (int i = 0; i < 10; i++)
            d.makePizza();

        ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(19);
        //executor.execute(d);
        for (int i = 1; i <= 20; i++)
            executor.execute(d);
        executor.shutdown();
/*        for (int i = 1; i <= 20; i++)
           d.servePizza();*/
    }
}
