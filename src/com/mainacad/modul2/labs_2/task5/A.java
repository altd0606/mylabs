package com.mainacad.modul2.labs_2.task5;

public class A {

    public double calcSquare(double sideA, double sideB){
        System.out.println("Это прямоугольник");
        return (sideA*sideB);
    }

    public double calcSquare(double sideA, double sideB, double sideC){
        double p = (sideA+sideB+sideC)/2;
        double rezult = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
        System.out.println("Это триугольник");
        return rezult;
    }

    public double calcSquare(double side){
        System.out.println("Это квадрат");
        return (side*side);
    }

    public double calcSquare(int radius){
        System.out.println("Это круг");
        return (double) (3.14*(radius*radius));
    }

    public void finalTest(final int someNum){

        System.out.println(someNum*someNum);
    }


}
