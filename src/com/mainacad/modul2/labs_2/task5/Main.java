package com.mainacad.modul2.labs_2.task5;

public class Main {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.calcSquare(4));
        System.out.println(a.calcSquare(4.0));
        System.out.println(a.calcSquare(3,3,3));
        System.out.println(a.calcSquare(12,10));
        a.finalTest(2);
    }
}
