package com.mainacad.modul2.labs_2.task3;

public class Employee {

    public void calcSalary(String name, double... salary){
        double totalSalary = 0.0;
        for (double salaryPerDay:salary){
            totalSalary+=salaryPerDay;
        }
        System.out.println("Name: " + name);
        System.out.println("Salary: " + totalSalary);
    }
}
