package com.mainacad.modul2.labs_2.task4;

public class Accountant {
    Accountant(String firstName,String lastName){
        Person person = new Person();
        person.setPerson(firstName,lastName);
    }

    Accountant(String firstName,String lastName,int age,int phoneNumber,String gender){
        Person person = new Person();
        person.setPerson(firstName,lastName,age,phoneNumber,gender);
    }
}
