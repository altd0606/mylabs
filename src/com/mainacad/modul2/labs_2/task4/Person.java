package com.mainacad.modul2.labs_2.task4;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String gender;
    private int phoneNumber;

    public void setPerson(String firstName,String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setPerson(int age,int phoneNumber){
        this.age = age;
        this.phoneNumber = phoneNumber;
    }

    public void setPerson(String gender){
        this.gender = gender;
    }

    public void setPerson(int phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public void setPerson(String firstName,String lastName,int age,int phoneNumber,String gender){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
    }

}
