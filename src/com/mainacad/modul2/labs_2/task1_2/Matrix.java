package com.mainacad.modul2.labs_2.task1_2;

public class Matrix {

    public void fillMatrix(int[][] inputArrays){
        for (int i = 0;i<inputArrays.length;i++){
            for (int j = 0;j<inputArrays[i].length;j++){
                inputArrays[i][j] = (int)(Math.random()*5);
            }
        }
    }

    public boolean checkBounds(int[][] firstMatrix,int[][] secondMatrix){
        if ((firstMatrix.length!=secondMatrix.length) || (firstMatrix[0].length!=secondMatrix[0].length)){
            System.out.println("Matrix has different bounds");
            return false;
        }
        else {
            return true;
        }
    }

    public void printArrays(int[][] arrayToOutput){
        for (int i = 0; i<arrayToOutput.length;i++){
            for (int j = 0; j<arrayToOutput[i].length;j++){
                System.out.print(arrayToOutput[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void additionMatrixs(int[][] firstMatrix,int[][] secondMatrix){

        int[][] rezultMatrix = new int[firstMatrix.length][firstMatrix[0].length];

        for (int i = 0;i<firstMatrix.length;i++){
            for (int j = 0;j<firstMatrix[i].length;j++){
                rezultMatrix[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
            }
        }

        printArrays(rezultMatrix);
    }

    public void multiplicationMatrixs(int[][] firstMatrix,int[][] secondMatrix){

        checkBounds(firstMatrix,secondMatrix);

        int[][] rezultMatrix = new int[firstMatrix.length][firstMatrix[0].length];

        for (int i = 0;i<firstMatrix.length;i++){
            for (int j = 0;j<firstMatrix[i].length;j++){
                rezultMatrix[i][j] = firstMatrix[i][j] * secondMatrix[i][j];
            }
        }

        printArrays(rezultMatrix);
    }
}
