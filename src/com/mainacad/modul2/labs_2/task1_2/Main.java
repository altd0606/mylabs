package com.mainacad.modul2.labs_2.task1_2;

import com.mainacad.modul2.labs_2.task1_2.Matrix;
import com.mainacad.modul2.labs_2.task1_2.RunClassMatrix;

public class Main {
    public static void main(String[] args) {

        Matrix matrix = new Matrix();

        int[][] matrixOne = new int[4][4];
        int[][] matrixTwo = new int[4][4];

        matrix.fillMatrix(matrixOne);
        matrix.fillMatrix(matrixTwo);


        matrix.printArrays(matrixOne);
        System.out.println();
        matrix.printArrays(matrixTwo);

        System.out.println();

        if (matrix.checkBounds(matrixOne,matrixTwo)){
            matrix.additionMatrixs(matrixOne,matrixTwo);
            System.out.println();
            matrix.multiplicationMatrixs(matrixOne,matrixTwo);
        }

        System.out.println();

        RunClassMatrix runClassMatrix = new RunClassMatrix(4,2);

    }
}
