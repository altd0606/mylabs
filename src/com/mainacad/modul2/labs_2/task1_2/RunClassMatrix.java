package com.mainacad.modul2.labs_2.task1_2;

public class RunClassMatrix {
    private int[][] firsMatrix;
    private int[][] secondMatrix;

    RunClassMatrix(int numRows, int numCols){
        firsMatrix = new int[numRows][numCols];
        secondMatrix = new int[numRows][numCols];

        Matrix matrix = new Matrix();

        matrix.fillMatrix(firsMatrix);
        matrix.fillMatrix(secondMatrix);

        matrix.additionMatrixs(firsMatrix,secondMatrix);
        System.out.println();
        matrix.multiplicationMatrixs(firsMatrix,secondMatrix);

    }
}
