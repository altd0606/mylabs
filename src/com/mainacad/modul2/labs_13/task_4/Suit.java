package com.mainacad.modul2.labs_13.task_4;

public enum Suit {
    SPADE,
    DIAMOND,
    CLUB,
    HEART
}
