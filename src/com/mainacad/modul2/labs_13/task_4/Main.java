package com.mainacad.modul2.labs_13.task_4;

public class Main {
    public static void main(String[] args) {
        Card[] cardDeck = new Card[52];

        int lastIndex = 0;
        for (Suit suit: Suit.values()){
            for (Rank rank: Rank.values()){
                cardDeck[lastIndex++] = new Card(suit,rank);
            }
        }

        for (Card cardInDeck:cardDeck) {
            System.out.println(cardInDeck);
        }
    }
}
