package com.mainacad.modul2.labs_13.task_4;

public class Card {
    private Suit cardSuit;
    private Rank cardRank;

    Card(Suit cardSuit, Rank cardRank){
        this.cardSuit = cardSuit;
        this.cardRank = cardRank;
    }

    @Override
    public String toString() {
        return "The Card: " +  cardSuit + "_" + cardRank;
    }

}
