package com.mainacad.modul2.labs_13.task_1_2_3;

public enum MyDayOfTheWeek {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;

    public MyDayOfTheWeek nextDay(){
        MyDayOfTheWeek[] dayOfTheWeek = MyDayOfTheWeek.values();

        int index = ordinal() == dayOfTheWeek.length-1 ? 0 : ordinal()+1;

        return dayOfTheWeek[index];
    }
}
