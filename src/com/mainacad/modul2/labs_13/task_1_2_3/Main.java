package com.mainacad.modul2.labs_13.task_1_2_3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        for (MyDayOfTheWeek dayOfTheWeek : MyDayOfTheWeek.values()) {
            System.out.println(dayOfTheWeek);
        }

        System.out.println();

        for (MyDayOfTheWeek dayOfTheWeek : MyDayOfTheWeek.values()) {
            switch (dayOfTheWeek){
                case THURSDAY: case TUESDAY:
                    System.out.println("My Java Day: " + dayOfTheWeek);
                    break;
                default:
                    System.out.println("It is not a Java day");
            }
        }
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Input day of the week:");
        String input = scanner.next();
        scanner.close();

        try {
            MyDayOfTheWeek dayOfTheWeek = MyDayOfTheWeek.valueOf(input.toUpperCase());
            System.out.println(dayOfTheWeek.nextDay());
        }catch (IllegalArgumentException e){
            System.out.println("Illegal day of the week");
        }

    }





}
