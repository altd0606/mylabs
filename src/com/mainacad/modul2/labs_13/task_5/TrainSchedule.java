package com.mainacad.modul2.labs_13.task_5;

import java.util.Scanner;

public class TrainSchedule {
     class Train{
        private int number;
        private String stationDispatch;
        private String stationArrival;
        private String timeDispatch;
        private String timeArrival;
        private DayOfWeek day;

        public DayOfWeek getDay() {
            return day;
        }

        public void setDay(DayOfWeek day) {
            this.day = day;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public String getStationDispatch() {
            return stationDispatch;
        }

        public void setStationDispatch(String stationDispatch) {
            this.stationDispatch = stationDispatch;
        }

        public String getStationArrival() {
            return stationArrival;
        }

        public void setStationArrival(String stationArrival) {
            this.stationArrival = stationArrival;
        }

        public String getTimeDispatch() {
            return timeDispatch;
        }

        public void setTimeDispatch(String timeDispatch) {
            this.timeDispatch = timeDispatch;
        }

        public String getTimeArrival() {
            return timeArrival;
        }

        public void setTimeArrival(String timeArrival) {
            this.timeArrival = timeArrival;
        }

        Train(int number,String stationDispatch,String stationArrival,String timeDispatch,String timeArrival, DayOfWeek dayOfWeek){
            setNumber(number);
            setStationDispatch(stationDispatch);
            setStationArrival(stationArrival);
            setTimeDispatch(timeDispatch);
            setTimeArrival(timeArrival);
            setDay(dayOfWeek);
        }

        @Override
        public String toString(){
            return "Info about train : " + number +
                    ", " + stationDispatch +
                    ", " + stationArrival +
                    ", " + timeDispatch +
                    ", " + timeArrival +
                    ", " + day;
        }
    }

    private Train[] trains;

    TrainSchedule(int numbersOfTrain){
        trains = new Train[numbersOfTrain];
    }

    private Train parseLineToTrain(String inputLine){
        String[] parseString = inputLine.split(",");
        Train train = new Train(Integer.valueOf(parseString[0]),parseString[1],parseString[2],parseString[3],parseString[4],DayOfWeek.valueOf(parseString[5].toUpperCase()));
        return train;
    }

    public void addTrain(){
        System.out.println("Enter new train to schedulere on next line (q - exit)");
        Scanner scanner = new Scanner(System.in);
        while (true){
            String inputLine = scanner.nextLine();
            if (!inputLine.equals("q")){
                for (int indexInTrainsArr = 0; indexInTrainsArr < trains.length; indexInTrainsArr++){
                    if (trains[indexInTrainsArr]==null){
                        trains[indexInTrainsArr] = parseLineToTrain(inputLine);
                        break;
                    }
                }
            }
            else {
                break;
            }
        }
        scanner.close();
    }

    public void printTrains(){
        for (int indexInTrainsArr = 0; indexInTrainsArr < trains.length; indexInTrainsArr++){
            if (trains[indexInTrainsArr]==null){
                break;
            }
            else {
                System.out.println(trains[indexInTrainsArr].toString());
            }
        }
    }

    public void searchTrain(String stationDispatch, String stationArrival, DayOfWeek dayOfTheWeek){
        for (int i = 0; i < trains.length; i++){
            if (dayOfTheWeek.nextDay()==trains[i].getDay() && stationArrival.equals(trains[i].getStationArrival() )&& stationDispatch.equals(trains[i].getStationDispatch())){
                System.out.println(trains[i]);
                //break;
            }
        }
    }

}
