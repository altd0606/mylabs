package com.mainacad.modul2.labs_13.task_5;

public enum DayOfWeek {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;

    public DayOfWeek nextDay(){
        DayOfWeek[] dayOfTheWeek = DayOfWeek.values();

        int index = ordinal() == dayOfTheWeek.length-1 ? 0 : ordinal()+1;

        return dayOfTheWeek[index];
    }
}
