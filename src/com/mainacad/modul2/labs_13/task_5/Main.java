package com.mainacad.modul2.labs_13.task_5;

public class Main {
    public static void main(String[] args) {
        TrainSchedule trainSchedule = new TrainSchedule(4);
        trainSchedule.addTrain();
        trainSchedule.printTrains();
        trainSchedule.searchTrain("A","B",DayOfWeek.SUNDAY);
/*1,A,B,10:30,13:00,MONDAY
2,B,A,13:30,16:00,THURSDAY
3,C,E,17:00,22:00,FRIDAY
4,E,A,6:00,23:00,SATURDAY
        */
    }
}
