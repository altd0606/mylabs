package com.mainacad.modul2.labs_3;

public class Main {
    public static void main(String[] args) {
        MyWindow myWindow = new MyWindow();
        myWindow.printFields();

        System.out.println();

        MyWindow myWindow1 = new MyWindow(10,10);
        myWindow1.printFields();
        System.out.println();

        MyWindow[] arrayOfWindows = new MyWindow[3];

        arrayOfWindows[0] = new MyWindow(13,14);
        arrayOfWindows[1] = new MyWindow(16.0,11.987654,6,"Some color");
        arrayOfWindows[2] = new MyWindow(17,20.999,4,"Red",false);

        for (int i = 0; i < arrayOfWindows.length;i++){
            arrayOfWindows[i].printFields();
            System.out.println();
        }



    }
}
