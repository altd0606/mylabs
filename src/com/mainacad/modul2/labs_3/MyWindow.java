package com.mainacad.modul2.labs_3;

public class MyWindow {

    private double width;
    private double height;
    private int numberOfGlass;
    private String color;
    private boolean isOpen;

    public MyWindow(){
        width = 100;
        height = 100;
        numberOfGlass = 3;
        color = "Mirrored";
        isOpen = true;
    }

    public MyWindow(double width,double height){
        this();
        this.width = width;
        this.height = height;
    }

    public MyWindow(double width,double height,int numberOfGlass,String color){
        this();
        this.width = width;
        this.height = height;
        this.numberOfGlass = numberOfGlass;
        this.color = color;
    }


    public MyWindow(double width,double height,int numberOfGlass,String color,boolean isOpen){
        this.width = width;
        this.height = height;
        this.numberOfGlass = numberOfGlass;
        this.color = color;
        this.isOpen = isOpen;
    }

    public String toString(){
        return "Weight : " + width + "\n"
                + "Height : " + height + "\n"
                + "Number of glass : " + numberOfGlass + "\n"
                + "Color : " + color + "\n"
                + "Status of opening : " + isOpen;
    }


    public void printFields(){
        System.out.println(toString());
    }
}
