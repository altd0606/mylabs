package com.mainacad.modul2.labs_10.task_3_4_5;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

abstract class Shape implements Drawable {
    private String shapeColor;

    String getShapeColor() {
        return shapeColor;
    }

    void setShapeColor(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    @Override
    public String toString(){
        return "Shape {" +
                "shapeColor = " + shapeColor +
                "}";
    }

    abstract double calcArea();

    public void draw(){
        System.out.println(toString());
    }

    private static boolean isNumber(String input){
        Pattern pattern = Pattern.compile("((-|\\\\+)?[0-9]+(\\\\.[0-9]+)?)+");
        Matcher isNumber = pattern.matcher(input);
        return isNumber.matches();
    }

    public static Shape parseShape(String input){
        String[] string = input.split(":|,");


        String typeOfShape = string[0].toLowerCase();

        switch (typeOfShape){
            case "rectangle":
/*                if (string.length!=4){
                    return null;
                }
                else if (isNumber(Double.valueOf(string[2]))&&isNumber(string[3])){
                    return null;
                }*/
                Rectangle rectangle = new Rectangle(string[1],Double.parseDouble(string[2]),Double.parseDouble(string[3]));
                return rectangle;

            case "triangle":
                Triangle triangle = new Triangle(string[1],Double.parseDouble(string[2]),Double.parseDouble(string[3]),Double.parseDouble(string[4]));
                return triangle;

            case "circle":
                Circle circle = new Circle(string[1],Double.parseDouble(string[2]));
                return circle;

            default:
                System.out.println("Ellegal input");
                return null;
        }
    }
}
