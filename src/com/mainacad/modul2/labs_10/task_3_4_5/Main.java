package com.mainacad.modul2.labs_10.task_3_4_5;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        System.out.println("Input number of shape");
        Scanner scanner = new Scanner(System.in);
        int numOfShapes = scanner.nextInt();


        Shape[] arrOfShapes = new Shape[numOfShapes];

        System.out.println("Enter shape on new line (q - exit)");

        while (true){

            String input = scanner.nextLine();
            if (input.equals("q")){break;}

            if (Shape.parseShape(input)!=null){
                for (int i = 0; i < arrOfShapes.length; i++){
                    if (arrOfShapes[i]==null){
                        arrOfShapes[i]=Shape.parseShape(input);
                        break;
                    }
                }
            }
        }

        scanner.close();

        for (Shape shape:arrOfShapes){
            shape.draw();
        }

        //Shape.parseShape("Rectangle:Red:1,2");


    }

}
