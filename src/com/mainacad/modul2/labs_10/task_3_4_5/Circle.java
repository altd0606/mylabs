package com.mainacad.modul2.labs_10.task_3_4_5;

public class Circle extends Shape implements Comparable {
    private double radius;

    Circle(String shapeColor, double radius){
        setShapeColor(shapeColor);
        this.radius = radius;
    }

    @Override
    public double calcArea(){
       // System.out.print("Shape area is: ");
        return Math.PI*(radius*radius);
    }

    @Override
    public String toString(){
        return "Circle {" +
                "shapeColor = " + getShapeColor() +
                ", radius = " + radius +
                "}";
    }

    @Override
    public void draw(){
        System.out.println(toString() + " Area is: " + calcArea());
    }

    @Override
    public int compareTo(Object o){
        Circle circle = (Circle) o;
        if (this.calcArea()>circle.calcArea())return 1;
        if (this.calcArea()<circle.calcArea())return -1;
        return 0;
    }

}
