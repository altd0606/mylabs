package com.mainacad.modul2.labs_10.task_3_4_5;

public class Triangle extends Shape implements Comparable{
    private double a,b,c;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double calcArea(){
        double s = (a+b+c)/2;
        //System.out.print("Shape area is: ");
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }

    @Override
    public String toString(){
        return "Triangle {" +
            "shapeColor = " + getShapeColor() +
                    ", a = " + a +
                    ", b = " + b +
                    ", c = " + c +
                    "}";
    }

    Triangle(String shapeColor, double a, double b,double c){
        setShapeColor(shapeColor);
        setA(a);
        setB(b);
        setC(c);

    }

    @Override
    public void draw(){
        System.out.println(toString() + " Area is: " + calcArea());
    }

    @Override
    public int compareTo(Object o){
        Triangle triangle = (Triangle)o;
        if (this.calcArea()>triangle.calcArea()) return 1;
        if (this.calcArea()<triangle.calcArea()) return -1;
        return 0;
    }
}
