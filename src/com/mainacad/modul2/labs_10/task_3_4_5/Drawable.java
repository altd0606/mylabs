package com.mainacad.modul2.labs_10.task_3_4_5;

interface Drawable {
    void draw();
}
