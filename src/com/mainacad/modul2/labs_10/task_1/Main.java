package com.mainacad.modul2.labs_10.task_1;

public class Main {
    public static void main(String[] args) {
        Integer i1 = 10;
        Integer i2 = new Integer(10);
        Integer i3 = Integer.valueOf(10);
        Integer i4 = Integer.parseInt("10");
        Integer[] arr = {i1,i2,i3,i4};

        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr.length; j++){
                boolean b = arr[i]==arr[j];
                System.out.println("==" + " " +i + " " + j + " "+ b);

                b = arr[i].equals(arr[j]);

                System.out.println("equals" + " " +i + " " + j + " "+ b);
            }
            System.out.println();
        }

        System.out.println("-----------------------------");

        i1 = 130;
        i2 = new Integer(130);
        i3 = Integer.valueOf(130);
        i4 = Integer.parseInt("130");
        Integer[] arr1 = {i1,i2,i3,i4};

        for (int i = 0; i < arr1.length; i++){
            for (int j = 0; j < arr1.length; j++){
                boolean b = arr1[i]==arr1[j];
                System.out.println("==" + " " +i + " " + j + " "+ b);

                b = arr1[i].equals(arr1[j]);

                System.out.println("equals" + " " +i + " " + j + " "+ b);
            }
            System.out.println();
        }
    }
}
