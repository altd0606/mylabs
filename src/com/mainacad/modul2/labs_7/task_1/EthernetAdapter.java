package com.mainacad.modul2.labs_7.task_1;

import java.util.Objects;

public class EthernetAdapter extends Device {
    private int speed;
    private String mac;

    public int getSpeed() {
        return speed;
    }

    public String getMac() {
        return mac;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return "EthernetAdapter{" +
                "speed=" + speed +
                ", mac=" + mac  +
                ", serialNumber=" + getSerialNumber() +
                ", manufacture=" + getManufacture() +
                ", price=" + String.format("%.2f",getPrice()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EthernetAdapter ethernetAdapter = (EthernetAdapter) o;
        return speed == ethernetAdapter.speed &&
                mac.equals(ethernetAdapter.mac) &&
                getPrice() == ethernetAdapter.getPrice() &&
                getManufacture() == ethernetAdapter.getManufacture() &&
                getSerialNumber() == ethernetAdapter.getSerialNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), speed, mac);
    }
}
