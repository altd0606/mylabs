package com.mainacad.modul2.labs_7.task_1;

import javax.print.DocFlavor;
import java.util.Objects;

public class Device {
    private String manufacture;
    private float price;
    private String serialNumber;

    public String getManufacture() {
        return manufacture;
    }

    public float getPrice() {
        return price;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString(){
        return "Device{" +
                "manufacture=" + manufacture +
                ", price = " + String.format("%.2f",price) +
                ", serialNumber = " + serialNumber +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return Float.compare(device.price, price) == 0 &&
                manufacture.equals(device.manufacture) &&
                serialNumber.equals(device.serialNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacture, price, serialNumber);
    }
}
