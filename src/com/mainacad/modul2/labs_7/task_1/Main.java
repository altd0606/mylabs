package com.mainacad.modul2.labs_7.task_1;

public class Main {
    public static void main(String[] args) {
        Device[] devices = new Device[2];
        Monitor monitor =new Monitor();
        monitor.setResolutionX(100);
        monitor.setResolutionY(100);
        monitor.setManufacture("Super M");
        monitor.setSerialNumber("1");
        monitor.setPrice(12.59F);

        EthernetAdapter ethernetAdapter = new EthernetAdapter();
        ethernetAdapter.setMac("0000 0000 0000");
        ethernetAdapter.setSpeed(100);
        ethernetAdapter.setManufacture("MunfBB");
        ethernetAdapter.setSerialNumber("2");
        ethernetAdapter.setPrice(113.44F);

        devices[0] = monitor;
        devices[1] = ethernetAdapter;

        for (Device device:devices) {
            System.out.println(device.toString());
        }

    }
}
