package com.mainacad.modul2.labs_7.task_1;

import java.util.Objects;

public class Monitor extends Device {
    private int resolutionX;
    private int resolutionY;

    public int getResolutionX() {
        return resolutionX;
    }

    public int getResolutionY() {
        return resolutionY;
    }

    public void setResolutionX(int resolutionX) {
        this.resolutionX = resolutionX;
    }

    public void setResolutionY(int resolutionY) {
        this.resolutionY = resolutionY;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "resolutionX=" + resolutionX +
                ", resolutionY=" + resolutionY +
                ", serialNumber=" + getSerialNumber() +
                ", manufacture=" + getManufacture() +
                ", price=" + String.format("%.2f",getPrice()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Monitor monitor = (Monitor) o;
        return resolutionX == monitor.resolutionX &&
                resolutionY == monitor.resolutionY &&
                getSerialNumber() == monitor.getSerialNumber() &&
                getManufacture() == monitor.getManufacture() &&
                getPrice() == monitor.getPrice();
    }

    @Override
    public int hashCode() {
        return Objects.hash(resolutionX, resolutionY,getManufacture(),getPrice(),getPrice());
    }
}
