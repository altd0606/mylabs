package com.mainacad.modul2.labs_7.task_2;

public class Circle extends Shape{
    private double radius;
    Circle(){}

    Circle(String shapeColor, double radius){
        setShapeColor(shapeColor);
        this.radius = radius;
    }

    @Override
    public double calcArea(){
       // System.out.print("Shape area is: ");
        return Math.PI*(radius*radius);
    }

    @Override
    public String toString(){
        return "Circle {" +
                "shapeColor = " + getShapeColor() +
                ", radius = " + radius +
                "}";
    }

}
