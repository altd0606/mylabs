package com.mainacad.modul2.labs_7.task_2;

public class Rectangle extends Shape{
    private double width;
    private double height;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    private Rectangle(){}

    Rectangle(String shapeColor, double width, double height){
        setShapeColor(shapeColor);
        setWidth(width);
        setHeight(height);
    }

    @Override
    public String toString(){
        return "Rectangle {" +
                "shapeColor = " + getShapeColor() +
                ", width = " + width +
                ", height = " + height +
                "}";
    }

    @Override
    public double calcArea(){
        //System.out.print("Shape area is: ");
        return height*width;
    }


}
