package com.mainacad.modul2.labs_7.task_2;

public class Shape {
    private String shapeColor;

    String getShapeColor() {
        return shapeColor;
    }

    void setShapeColor(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    Shape(){}
    Shape(String shapeColor){
        this.shapeColor = shapeColor;
    }

    @Override
    public String toString(){
        return "Shape {" +
                "shapeColor = " + shapeColor +
                "}";
    }

    public double calcArea(){
        System.out.print("Shape area is: ");
        return 0.0;
    }
}
