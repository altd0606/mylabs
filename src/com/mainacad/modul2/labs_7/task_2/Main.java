package com.mainacad.modul2.labs_7.task_2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String color = "red";
        double sumArea = 0.0;
        double sumRectArea = 0.0;
        double sumTriangleArea = 0.0;
        double sumCircleArea = 0.0;

        Shape shape = new Shape("red");
        System.out.println(shape.toString());
        System.out.println(shape.calcArea());

        Circle circle = new Circle("Blue",22);
        System.out.println(circle.toString());
        System.out.println(circle.calcArea());

        Rectangle rectangle = new Rectangle("Green",4,4);
        System.out.println(rectangle.toString());
        System.out.println(rectangle.calcArea());

        Triangle triangle = new Triangle("Yellow",2,2,2);
        System.out.println(triangle.toString());
        System.out.println(triangle.calcArea());

        System.out.println();

        Shape[] arr = new Shape[9];
        int i = 0;
        for (;i<5;i++){
/*            Scanner scanner = new Scanner(System.in);
            System.out.println("Input color: ");
            String inputColor = scanner.nextLine();*/
            Rectangle rectangle1 = new Rectangle(color,Math.random()*100,Math.random()*100);
            arr[i] = rectangle1;
        }
        for (;i<7;i++){
/*            Scanner scanner = new Scanner(System.in);
            System.out.println("Input color: ");
            String inputColor = scanner.nextLine();*/
            Circle circle1 = new Circle(color,Math.random()*100);
            arr[i] = circle1;
        }
        for (;i<9;i++){
/*            Scanner scanner = new Scanner(System.in);
            System.out.println("Input color: ");
            String inputColor = scanner.nextLine();*/
            Triangle triangle1 = new Triangle(color,Math.random()*100,Math.random()*100,Math.random()*100);
            arr[i] = triangle1;
        }

        for (Shape shape1:arr) {
            System.out.println(shape1.toString());
            System.out.println(shape1.calcArea() + "\n");
        }

        for (Shape shape1:arr){
            if (!Double.isNaN(shape1.calcArea())){
                sumArea+=shape1.calcArea();
            }

            if (shape1 instanceof Rectangle){
                sumRectArea+=shape1.calcArea();
            }
            if (shape1 instanceof Triangle && (!Double.isNaN(shape1.calcArea()))){
                sumTriangleArea+=shape1.calcArea();
            }
            if (shape1 instanceof Circle){
                sumCircleArea+=shape1.calcArea();
            }
        }

        System.out.println("Total area: " + sumArea);
        System.out.println("Rectangle total area: " + sumRectArea);
        System.out.println("Triangle total area: " + sumTriangleArea);
        System.out.println("Circle total area: " + sumCircleArea);


    }
}
