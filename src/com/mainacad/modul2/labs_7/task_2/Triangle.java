package com.mainacad.modul2.labs_7.task_2;

public class Triangle extends Shape {
    private double a,b,c;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double calcArea(){
        double s = (a+b+c)/2;
        //System.out.print("Shape area is: ");
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }

    @Override
    public String toString(){
        return "Triangle {" +
            "shapeColor = " + getShapeColor() +
                    ", a = " + a +
                    ", b = " + b +
                    ", c = " + c +
                    "}";
    }

    Triangle(String shapeColor, double a, double b,double c){
        setShapeColor(shapeColor);
        setA(a);
        setB(b);
        setC(c);

    }
}
