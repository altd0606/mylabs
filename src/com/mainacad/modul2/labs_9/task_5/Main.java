package com.mainacad.modul2.labs_9.task_5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static boolean checkPersonWithRegExp(String userName){
        String emplat = "^[A-Z][a-z]*";
        Pattern pattern = Pattern.compile(emplat);
        Matcher m = pattern.matcher(userName);
        return m.matches();
    }
    public static void main(String[] args) {
        String[] arr = {"VOVA","Ivan","R2d2","ZX","Anna","12345","ToAd","TomCat"," "};


        for (String input:arr){

            if (checkPersonWithRegExp(input)){
                System.out.println(input);
            }
            else {
                System.out.println("Not a name");
            }

        }


    }
}
