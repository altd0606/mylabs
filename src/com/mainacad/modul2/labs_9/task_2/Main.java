package com.mainacad.modul2.labs_9.task_2;

public class Main {
    public static void main(String[] args) {
        String myStr1 = "Cartoon";
        String myStr2 = "Tomcat";

        StringBuilder rezultStr = new StringBuilder();

        for (int i = 0;i < myStr1.length();i++){
            char flag = myStr1.charAt(i);
            if (myStr2.indexOf(flag) < 0){
                rezultStr.append(flag);
            }
        }

        System.out.println(rezultStr);

    }
}
