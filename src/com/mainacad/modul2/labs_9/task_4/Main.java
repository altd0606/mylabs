package com.mainacad.modul2.labs_9.task_4;

public class Main {
    public static void main(String[] args) {
        String myStr = "This is String, split by StringTokenizer. Created by Student:Name of Student";

        for (String rezult:myStr.split(" ")){
            System.out.println(rezult);
        }
        System.out.println("---------------");
        for (String rezult:myStr.split(",")){
            System.out.println(rezult);
        }
        System.out.println("------------------");
        for (String rezult:myStr.split("\\.")){
            System.out.println(rezult);
        }
    }
}
