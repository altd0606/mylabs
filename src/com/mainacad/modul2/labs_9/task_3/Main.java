package com.mainacad.modul2.labs_9.task_3;

import java.util.Arrays;

public class Main {
    public static char[] uniqueChars(String input){
        char[] rezult = new char[0];

        for (int i = 0; i < input.length(); i++){
            int count = 0;
            for (int j = 0; j < input.length(); j++){
                if (input.toLowerCase().charAt(i)==input.toLowerCase().charAt(j)){
                    count++;
                }
            }
            if (count==1){
                rezult = Arrays.copyOf(rezult,rezult.length+1);
                rezult[rezult.length-1]=input.charAt(i);
            }
        }

        return rezult;
    }
    public static void main(String[] args) {
        System.out.println(uniqueChars("Using methods of class String"));

    }
}
