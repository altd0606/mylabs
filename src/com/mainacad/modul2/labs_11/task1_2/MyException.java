package com.mainacad.modul2.labs_11.task1_2;

public class MyException extends Exception {
    private String message;
    public MyException(String message){
        //super(message);
        this.message = message;
    }

    public void printMessage(){
        System.out.println(this.message);
    }
}
