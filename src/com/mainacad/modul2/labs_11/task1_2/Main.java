package com.mainacad.modul2.labs_11.task1_2;

public class Main {
    public static void main(String[] args){
        try {
            System.out.println("Try block");
            //throw new Exception("Some exception");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("That was finally block");
        }

    }
}
