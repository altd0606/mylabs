package com.mainacad.modul2.labs_11.task1_2;

public class MainMyException {
    public static void main(String[] args){
        try {
            throw new MyException("Some message123");
        }catch (MyException e){
            //System.out.println(e.getMessage());
            e.printMessage();
        }

/*        MyTest myTest = new MyTest();

        try {
            myTest.test();
        }catch (MyException e){
            e.printMessage();
        }*/

        MyTest myTest = null;

        try {
            myTest.test();
        }catch (MyException |
                NullPointerException e){
            System.out.println("OR exception");
            //e.printMessage();
        }


    }
}
