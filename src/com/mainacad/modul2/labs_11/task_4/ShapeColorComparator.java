package com.mainacad.modul2.labs_11.task_4;

import java.util.Comparator;

public class ShapeColorComparator implements Comparator {
    public int compare(Object o1, Object o2){
        String str1 = ((Shape)o1).getShapeColor();
        String str2 = ((Shape)o2).getShapeColor();

        return str1.compareTo(str2);
    }
}
