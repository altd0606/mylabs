package com.mainacad.modul2.labs_11.task_4;

interface Drawable {
    void draw();
}
