package com.mainacad.modul2.labs_11.task_4;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

abstract class Shape implements Drawable {
    private String shapeColor;

    Shape(){}

    String getShapeColor() {
        return shapeColor;
    }

    void setShapeColor(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    @Override
    public String toString(){
        return "Shape {" +
                "shapeColor = " + shapeColor +
                "}";
    }

    abstract double calcArea();

    public void draw(){
        System.out.println(toString());
    }

    private static boolean isNumber(String input){
        Pattern pattern = Pattern.compile("((-|\\\\+)?[0-9]+(\\\\.[0-9]+)?)+");
        Matcher isNumber = pattern.matcher(input);
        return isNumber.matches();
    }

    private static boolean isNumber2(String input){
        try {
            Double.valueOf(input);
        }catch (NumberFormatException e){
            return false;
        }
        return true;
    }

    public static Shape parseShape(String input) throws InvalidShapeStringException{
        String[] string = input.split(":|,");


        String typeOfShape = string[0].toLowerCase();
        Shape shape = null;

        switch (typeOfShape){
            case "rectangle":
                if (string.length!=4){
                    throw new InvalidShapeStringException();
                }
                else if (!isNumber2(string[2])||!isNumber2(string[3])){
                    throw new InvalidShapeStringException();
                }
                shape = new Rectangle(string[1],Double.parseDouble(string[2]),Double.parseDouble(string[3]));
                break;

            case "triangle":
                if (string.length!=5){
                    throw new InvalidShapeStringException();
                }
                else if (!isNumber(string[2])||!isNumber(string[3])||!isNumber(string[4])){
                    throw new InvalidShapeStringException();
                }
                shape = new Triangle(string[1],Double.parseDouble(string[2]),Double.parseDouble(string[3]),Double.parseDouble(string[4]));
                break;

            case "circle":
                if (string.length!=3){
                    throw new InvalidShapeStringException();
                }
                else if (!isNumber(string[2])){
                    throw new InvalidShapeStringException();
                }
                shape = new Circle(string[1],Double.parseDouble(string[2]));
                break;
            default:
                throw new InvalidShapeStringException();
        }
        return shape;
    }

    public static void userConsolInteraction(){
        System.out.println("Input number of shape");
        Scanner scanner = new Scanner(System.in);
        int numOfShapes = scanner.nextInt();

        Shape[] arrOfShapes = new Shape[numOfShapes];

        System.out.println("Enter shape on new line (q - exit)");

        while (true){

            String input = scanner.nextLine();

            if (input.equals("q")){break;}

            try {
                if (Shape.parseShape(input)!=null){
                    for (int i = 0; i < arrOfShapes.length; i++){
                        if (arrOfShapes[i]==null){
                            arrOfShapes[i]=Shape.parseShape(input);
                            break;
                        }
                    }
                }
            }catch (InvalidShapeStringException e){
                System.out.println("Invalid input");
            }
        }

        scanner.close();

        for (Shape shape:arrOfShapes){
            shape.draw();
        }
    }
}
