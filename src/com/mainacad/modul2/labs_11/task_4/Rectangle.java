package com.mainacad.modul2.labs_11.task_4;

public class Rectangle extends Shape implements Comparable{
    private double width;
    private double height;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


    Rectangle(String shapeColor, double width, double height){
        setShapeColor(shapeColor);
        setWidth(width);
        setHeight(height);
    }

    @Override
    public String toString(){
        return "Rectangle {" +
                "shapeColor = " + getShapeColor() +
                ", width = " + width +
                ", height = " + height +
                "}";
    }

    @Override
    public double calcArea(){
        //System.out.print("Shape area is: ");
        return height*width;
    }

    @Override
    public void draw(){
        System.out.println(toString() + " Area is: " + calcArea());
    }

    @Override
    public int compareTo(Object o){
        Rectangle rectangle = (Rectangle)o;
        if (this.calcArea()>rectangle.calcArea()) return 1;
        if (this.calcArea()<rectangle.calcArea()) return -1;
        return 0;
    }


}
