package com.mainacad.modul2.labs_11.task_3;

public class InvalidAgeException extends RuntimeException{
    InvalidAgeException(String message){
        super(message);
    }
}
