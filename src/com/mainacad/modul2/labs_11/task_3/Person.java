package com.mainacad.modul2.labs_11.task_3;

public class Person {
    private String firstName;
    private String lastName;
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return this.age;

    }

    public void setAge(int age) {
        if (age>0 && age<120){
            this.age = age;
        }
        else {
            throw new InvalidAgeException("Invalid age");
        }
    }


}
