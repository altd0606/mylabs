package com.mainacad.modul2.labs_1;

public class Computer {
    private String manufacture;
    private int serialNumber;
    private float price;
    private int quantityCPU;
    private int frequencyCPU;

    public Computer(){

    }

    public void setManufacture(String manufacture){
        this.manufacture = manufacture;
    }

    public String getManufacture(){
        return manufacture;
    }

    public void setSerialNumber(int serialNumber){
        this.serialNumber = serialNumber;
    }

    public int getSerialNumber(){
        return serialNumber;
    }

    public void setPrice(float price){
        this.price = price;
    }

    public float getPrice(){
        return price;
    }

    public void setQuantityCPU(int quantityCPU){
        this.quantityCPU = quantityCPU;
    }

    public int getQuantityCPU(){
        return quantityCPU;
    }

    public void setFrequencyCPU(int frequencyCPU){
        this.frequencyCPU = frequencyCPU;
    }

    public int getFrequencyCPU(){
        return frequencyCPU;
    }


    public String toString(){
        return "Computer{" +
                "manufacture=" + manufacture + '\'' +
                ", serialNumber=" + serialNumber   +
                ", price=" + String.format("%.2f", price) +
                ", qualityCPU=" + quantityCPU +
                ", frequencyCPU=" + frequencyCPU +
                '}';

    }

    static public void printArrayOfComputers(Computer[] computers){

        for (int i = 0; i<computers.length;i++){
            System.out.println(computers[i].toString());
        }

    }

}
