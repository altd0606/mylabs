package com.mainacad.modul2.labs_1;

public class Main {
    public static void main(String[] args) {
        float lastPrice = (float) (Math.random()*1000);
        Computer[] computersArray = new Computer[5];

        for (int i = 0; i<computersArray.length; i++){
            //Scanner scanner = new Scanner(System.in);
            /*System.out.print("Set manufacture of computer: ");
            computer.setManufacture(scanner.nextLine());*/

            Computer computer = new Computer();
            computer.setFrequencyCPU((int)(Math.random()*4+1));
            computer.setManufacture("ASUS");
            lastPrice = lastPrice+lastPrice*0.1F;
            computer.setPrice(lastPrice);
            computer.setSerialNumber((int)(Math.random()*100000));

            while (true){
                int randQualitiCpu = (int)(Math.random()*7+2);
                if (randQualitiCpu%2==0){
                    computer.setQuantityCPU(randQualitiCpu);
                    break;
                }
            }
            computersArray[i] = computer;
            //System.out.println(computer.toString());
        }

        //System.out.println();

        Computer.printArrayOfComputers(computersArray);

    }
}
