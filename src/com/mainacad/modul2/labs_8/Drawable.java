package com.mainacad.modul2.labs_8;

interface Drawable {
    void draw();
}
