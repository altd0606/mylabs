package com.mainacad.modul2.labs_8;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] color = {"Red","Yellow","Blue","Green","Black","White","Purple"};

        Shape[] arr = new Shape[9];
        int i = 0;
        for (;i<5;i++){
            Rectangle rectangle1 = new Rectangle(color[(int)(Math.random()*7)],Math.random()*100,Math.random()*100);
            arr[i] = rectangle1;
        }
        for (;i<7;i++){
            Circle circle1 = new Circle(color[(int)(Math.random()*7)],Math.random()*100);
            arr[i] = circle1;
        }
        for (;i<9;i++){
            Triangle triangle1 = new Triangle(color[(int)(Math.random()*7)],Math.random()*100,Math.random()*100,Math.random()*100);
            arr[i] = triangle1;
        }

        for (Shape shape:arr) {
            shape.draw();
        }

        System.out.println();

        Shape[] arr2 = new Shape[5];

        for (int j = 0; j < arr2.length; j++){
            Rectangle rectangle1 = new Rectangle(color[(int)(Math.random()*7)],Math.random()*100,Math.random()*100);
            arr2[j] = rectangle1;
        }

        Arrays.sort(arr2);

        for (Shape shape:arr2) {
            shape.draw();
        }

        System.out.println();

        Arrays.sort(arr,new ShapeColorComparator());

        for (Shape shape:arr) {
            shape.draw();
        }



    }
}
