package com.mainacad.modul2.labs_8;

abstract class Shape implements Drawable {
    private String shapeColor;

    String getShapeColor() {
        return shapeColor;
    }

    void setShapeColor(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    @Override
    public String toString(){
        return "Shape {" +
                "shapeColor = " + shapeColor +
                "}";
    }

    abstract double calcArea();

    public void draw(){
        System.out.println(toString());
    }
}
