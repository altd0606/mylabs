package com.mainacad.modul2.labs_15;

import java.util.*;

public class FillAndPrint {
    public static void fillArrayList(ArrayList<String> list,int size){
        for (int i = 0; i < size; i++){
            list.add("number_" + i);
        }
    }

    public static void fillLinkedList(LinkedList<String> list,int size){
        for (int i = 0; i < size; i++){
            list.add("num_" + (int)(Math.random()*list.size()));
        }
    }

    public static void printList(List list){
        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    public static void reversePrintList(List<String> list){
        ListIterator<String> listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()){
            System.out.println(listIterator.previous());
        }
    }
}
