package com.mainacad.modul2.labs_15.task_3;

import java.util.*;

class Mixer {
     static void randomMix(LinkedList<String> linkedList, ArrayList<String> arrayList){
        Iterator<String> iterator = arrayList.iterator();

        while (iterator.hasNext()){
            Random random = new Random();
            linkedList.add(random.nextInt(linkedList.size()),iterator.next());
        }
    }

    static void throughOneMix(LinkedList<String> linkedList, ArrayList<String> arrayList){
         ListIterator<String> listIterator = linkedList.listIterator(linkedList.size());
         Iterator<String> iterator = arrayList.iterator();

         while(listIterator.hasPrevious() && iterator.hasNext()){
             listIterator.add(iterator.next());
             listIterator.previous();
             listIterator.previous();
         }
    }

}
