package com.mainacad.modul2.labs_15.task_3;

import com.mainacad.modul2.labs_15.FillAndPrint;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        LinkedList<String> linkedList = new LinkedList<>();

        FillAndPrint.fillArrayList(arrayList,10);
        //FillAndPrint.printList(arrayList);
        //System.out.println();
        FillAndPrint.fillLinkedList(linkedList,10);
        //FillAndPrint.printList(linkedList);
        //System.out.println();

        Mixer.randomMix(linkedList,arrayList);
        FillAndPrint.printList(linkedList);

        Mixer.throughOneMix(linkedList,arrayList);
        FillAndPrint.printList(linkedList);

    }
}
