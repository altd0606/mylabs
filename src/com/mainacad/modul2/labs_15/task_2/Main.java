package com.mainacad.modul2.labs_15.task_2;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> list = new LinkedList<>();
        for (int i = 0; i < 10; i++){
            list.add("number_" + (int)(Math.random()*list.size()));
        }

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        //System.out.println(list.toString());
    }
}
