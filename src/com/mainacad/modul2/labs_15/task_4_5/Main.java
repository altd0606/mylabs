package com.mainacad.modul2.labs_15.task_4_5;

import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        MyNumGenerator myNumGenerator = new MyNumGenerator(5,5);
        List<Integer> list = myNumGenerator.generate();
        //FillAndPrint.printList(list);
        System.out.println(list.toString());
        Set<Integer> set = myNumGenerator.generateDistinct();
        System.out.println(set.toString());
    }
}
