package com.mainacad.modul2.labs_15.task_4_5;

import java.util.*;

public class MyNumGenerator {
    private int numOfElements;
    private int maxNumber;

    MyNumGenerator(int numOfElements, int maxNumber){
        this.numOfElements = numOfElements;
        this.maxNumber = maxNumber;
    }

    List<Integer> generate(){
        List<Integer> list = new ArrayList<>(numOfElements);

        for (int i = 0; i < numOfElements;i++){
            Random random = new Random();
            list.add(random.nextInt(maxNumber+1));
        }
        return list;
    }

    Set<Integer> generateDistinct(){
        Set<Integer> set = new HashSet<>(numOfElements);
        for (int i = 0; i < numOfElements;i++){
            Random random = new Random();
            set.add(random.nextInt(maxNumber+1));
        }
        return set;

    }
}
