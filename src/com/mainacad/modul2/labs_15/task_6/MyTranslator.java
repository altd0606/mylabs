package com.mainacad.modul2.labs_15.task_6;

import java.util.HashMap;

public class MyTranslator {
    private HashMap<String,String> dictionary;
    MyTranslator(){
        dictionary = new HashMap<>();
    }

    void addNewWord(String en, String ru){
        dictionary.put(en,ru);
    }

    String translate(String en){
        String[] inputWords = en.split(" ");
        String rezult = "";
        for (int i = 0; i < inputWords.length; i++){
            inputWords[i] = dictionary.get(inputWords[i]);
        }
        for (int i = 0; i < inputWords.length; i++){
            rezult += inputWords[i] + " ";
        }
        return rezult;
    }

}
