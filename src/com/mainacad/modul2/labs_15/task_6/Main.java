package com.mainacad.modul2.labs_15.task_6;

public class Main {
    public static void main(String[] args) {
        MyTranslator myTranslator = new MyTranslator();
        myTranslator.addNewWord("cat","кот");
        myTranslator.addNewWord("caught","поймал");
        myTranslator.addNewWord("mouse","мышь");

        System.out.println(myTranslator.translate("cat caught mouse"));

    }
}
