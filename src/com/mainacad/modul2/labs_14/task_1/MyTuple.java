package com.mainacad.modul2.labs_14.task_1;

public class MyTuple <A,B,C> {
    private A a;
    private B b;
    private C c;

    public A getA() {
        return a;
    }

    public B getB() {
        return b;
    }

    public C getC() {
        return c;
    }


    MyTuple(A a, B b, C c){
        this.a = a;
        this.b = b;
        this.c = c;

    }
}
