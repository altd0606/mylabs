package com.mainacad.modul2.labs_14.task_4;

public class Main {
    public static void main(String[] args) {
        Integer[] simpleNumber = {1,2,3,4,5,6,7,8,9};
        String[] numeOfNumber = {"one","two","tree","four","five","six","seven","eight","nine"};

        //MyMixer myMixer = new MyMixer(simpleNumber,Integer.class);
        //myMixer.shuffle(simpleNumber);
        MyMixer myMixer = new MyMixer(numeOfNumber,String.class);
        myMixer.shuffle(numeOfNumber);
    }
}
