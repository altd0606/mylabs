package com.mainacad.modul2.labs_14.task_4;

import java.lang.reflect.Array;

public class MyMixer<T> {
    private T[] arrays;
    private Class<T> type;

    MyMixer(T[] arrays,Class<T> type){
        this.arrays = arrays;
        this.type = type;
    }

    public T[] shuffle(T[] array){

        T[] rezultArr =  (T[])Array.newInstance(type, array.length);

        Integer[] listOfRandomIndex = new Integer[array.length];
        int randomIndex;

        for (int i = 0; i < array.length; i++){
            randomIndex = (int)(Math.random()*array.length);
            while (true){
                if (isNewRandomIndex(listOfRandomIndex,randomIndex)){
                    listOfRandomIndex[i] = randomIndex;
                    break;
                }
                randomIndex = (int)(Math.random()*array.length);
            }

            addInArr(rezultArr,array[randomIndex]);
        }
        printArr(rezultArr);
        return rezultArr;
    }

    private void addInArr(T[] array,T element){
        for (int i = 0; i < array.length; i++){
            if(array[i]==null){
                array[i] = element;
                break;
            }
        }
    }

    private boolean isNewRandomIndex(Integer[] arr,int randomIndex){
        for (int i = 0; i < arr.length; i++){
            try {
                if (arr[i]==randomIndex){
                    return false;
                }
            }catch (NullPointerException e){
                return true;
            }
        }
        return true;
    }

    public void printArr(T[] array){
        for (int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
    }
}
