package com.mainacad.modul2.labs_14.task_2_3;

import java.util.Comparator;

public class MyTestMethod {
    public static<T extends Number> int CalcNum(T[] array,T maxElement){
        int count = 0;
        for (T arr: array) {
            Comparator comparator = new Comparator() {
                @Override
                public int compare(Object o, Object t1) {
                    T inputVar = (T)o;
                    T inputVar2 = (T)t1;
                    if (inputVar.doubleValue()==inputVar2.doubleValue()){return 0;}
                    else if (inputVar.doubleValue()<inputVar2.doubleValue()){return -1;}
                    else {return 1;}
                }
            };
            if (comparator.compare(arr,maxElement)>0){
                count++;
            }
        }
        return count;
    }

    public static<T extends Number> Double calcSum(T[] array,T maxElement){
        Double sumToReturn = 0.0;
        for (T arr: array) {
            Comparator comparator = new Comparator() {
                @Override
                public int compare(Object o, Object t1) {
                    T inputVar = (T)o;
                    T inputVar2 = (T)t1;
                    if (inputVar.doubleValue()==inputVar2.doubleValue()){return 0;}
                    else if (inputVar.doubleValue()<inputVar2.doubleValue()){return -1;}
                    else {return 1;}
                }
            };
            if (comparator.compare(arr,maxElement)>0){
                sumToReturn+=arr.doubleValue();
            }
        }
        return sumToReturn;
    }
}
