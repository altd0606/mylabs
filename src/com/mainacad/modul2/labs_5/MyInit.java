package com.mainacad.modul2.labs_5;

import java.util.Random;

public class MyInit {
    private int id;
    private static  int nextId;

    private static int[] arr;

    static {
        Random random = new Random();
        nextId = random.nextInt(1000);

        arr = new int[10];
        for (int i = 0;i<arr.length;i++){
            arr[i] = (int)(100*Math.random());
        }
    }


    public MyInit(){
        nextId++;
        id = nextId;
    }

    public int getId(){
        return id;
    }

    public void printArray(){
        for (int i = 0;i<arr.length;i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
