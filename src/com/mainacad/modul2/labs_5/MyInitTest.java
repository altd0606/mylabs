package com.mainacad.modul2.labs_5;

public class MyInitTest {
    {
        System.out.println("Non-static 1");
    }
    static {
        System.out.println("static blook 1");
    }
    {
        System.out.println("Non-static 2");
    }
    static {
        System.out.println("static blook 2");
    }

    MyInitTest(){
        System.out.println("Constructor 1");
    }
    MyInitTest(int a){
        this();
        System.out.println("Constructor 2");
    }

}
