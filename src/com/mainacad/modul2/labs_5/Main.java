package com.mainacad.modul2.labs_5;

public class Main {
    public static void main(String[] args) {
        MyInitTest myInitTest = new MyInitTest(1);
        System.out.println();
        MyInitTest myInitTest1 = new MyInitTest();

        MyInit myInit = new MyInit();
        MyInit myInit1 = new MyInit();
        myInit.printArray();
        myInit1.printArray();

        MyInit[] myInitArr = new MyInit[5];
        for (int i = 0;i<myInitArr.length;i++){
            MyInit myInit2 = new MyInit();
            myInitArr[i] = myInit2;
        }
        for (int i = 0;i<myInitArr.length;i++){
            System.out.println(myInitArr[i].getId());
        }
    }
}
