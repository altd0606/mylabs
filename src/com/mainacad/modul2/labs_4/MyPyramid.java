package com.mainacad.modul2.labs_4;

import java.util.Scanner;

public class MyPyramid {
    public static void printPyramid(int height){

        if ((height<=9) && (height>=1)){
            buildPyramid(height);
        }
        else{
            Scanner scanner = new Scanner(System.in);

            do {
                System.out.print("Must be in range from 1 to 9 \nInput right height :");
                height = scanner.nextInt();
            }while ((height>9) || (height<1));

            buildPyramid(height);
        }
    }

    private static void buildPyramid(int height){
        int lenghtOfLastFloor = (height*2-1);
        int numOfNumber = 1;

        for (int i = 1;i<=height;i++){
            int numOfspace = (lenghtOfLastFloor-numOfNumber)/2;
            int outNum = 1;
            boolean flag = false;

            for (int s = 0;s<numOfspace;s++){
                System.out.print(" ");
            }
            for (int n = 0;n<numOfNumber;n++){
                System.out.print(outNum);


                if (flag){
                    outNum--;
                    flag = outNum==1 ? false : true;
                }
                else {
                    outNum++;
                    flag = outNum==i ? true : false;
                }

            }
            for (int s = 0;s<numOfspace;s++){
                System.out.print(" ");
            }
            System.out.println();
            numOfNumber += 2;
        }
    }
}
