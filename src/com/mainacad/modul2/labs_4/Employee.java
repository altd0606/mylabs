package com.mainacad.modul2.labs_4;

import java.util.Scanner;

public class Employee {
    private String firstName;
    private String lastName;
    private String occupation;
    private String telephone;
    public static int numberOfEmployeed;

    public Employee(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input firstName: ");
        String input = scanner.nextLine();
        setFirstName(input);

        System.out.println("Input lastName: ");
        input = scanner.nextLine();
        setLastName(input);

        System.out.println("Input occupation: ");
        input = scanner.nextLine();
        setOccupation(input);

        System.out.println("Input telephone: ");
        input = scanner.nextLine();
        setTelephone(input);

        numberOfEmployeed++;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public void setLastName(String lastName){
        this.telephone = lastName;
    }
    public void setOccupation(String occupation){
        this.occupation = occupation;
    }
    public void setTelephone(String telephone){
        this.telephone = telephone;
    }

    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public String getOccupation(){
        return occupation;
    }
    public String getTelephone(){
        return telephone;
    }


}
