package com.mainacad.modul2.labs_4;

public class GravityCalculator {
    private static final double ACCELERATION = -9.81;
    private static final double START_SPEED = 0.0;
    private static final double START_POSITION = 0.0;

    public static double calcDist(double time){
        return ((0.5*ACCELERATION*(time*time)) + (START_SPEED*time) + START_POSITION);
    }
}
