package com.mainacad.modul2.labs_4;

public class MyCalc {
    public static double calcPI(int n){
        double rezultPI = 4.0;
        boolean flag = true;
        double dividerNum = 3.0;

        for (int i = 0 ;i<=n;i++){
            if (flag){
                rezultPI-=(4.0/dividerNum);
                dividerNum+=2;
                flag = false;
            }
            else {
                rezultPI+=(4.0/dividerNum);
                dividerNum+=2;
                flag = true;
            }
        }

        return rezultPI;
    }
}
