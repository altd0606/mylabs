package com.mainacad.modul2.labs_4;

public class MyMath {
    private final static double PI = 3.14;

    public static int findMin(int[] inputArr){
        int minValue = Integer.MAX_VALUE;
        for (int i = 0;i<inputArr.length;i++){
            minValue = inputArr[i]<minValue ? inputArr[i] : minValue;
        }
        return  minValue;
    }

    public static int findMax(int[] inputArr){
        int maxValue = Integer.MIN_VALUE;
        for (int i = 0;i<inputArr.length;i++){
            maxValue = inputArr[i]>maxValue ? inputArr[i] : maxValue;
        }
        return  maxValue;
    }

    public static double areaOfCircle(double radius){
        return (radius*radius*PI);
    }
}
