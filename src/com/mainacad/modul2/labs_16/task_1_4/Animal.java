package com.mainacad.modul2.labs_16.task_1_4;

public class Animal {

}

class Dog extends Animal {

}

class Puppy extends Dog{

}

class Main{
    static void foo(Animal obj){
        System.out.println("Animal");
    }
    static void foo(Dog obj){
        System.out.println("Dog");
    }
    static void foo(Puppy obj){
        System.out.println("Puppy");
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Dog dog = new Dog();
        Puppy puppy = new Puppy();
        Animal animal1 = new Dog();
        Dog dog1 = new Puppy();
        Animal animal2 = new Puppy();
        foo(animal);
        foo(dog);
        foo(puppy);
        foo(animal1);
        foo(dog1);
        foo(animal2);
    }
}