package com.mainacad.modul2.labs_16.task_2_3;

public class Main {
    static void foo(int a){
        System.out.println("int");
    }
    static void foo(byte b){
        System.out.println("byte");
    }
    static void foo(Byte b){
        System.out.println("Byte");
    }
    static void foo(int a, int b){
        System.out.println("a = " + a + " b = " + b);
    }
    static void foo(int... a){
        for(int arr : a){
            System.out.print(arr + " ");
        }
    }
    public static void main(String[] args) {
        byte b =5;
        foo(b);
        System.out.println();

        foo(1,2);
        foo(1,2,3);
        System.out.println();
        foo(b);
        foo(5);
    }
}
