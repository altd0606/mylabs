package com.mainacad.modul2.labs_12.task1_2;

import java.util.Arrays;
import java.util.Comparator;



public class MyPhoneBook {
    static class PhoneNumber{
        private String name;
        private String phone;

        PhoneNumber(String name, String phone){
            this.name = name;
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        @Override
        public String toString() {
            return "PhoneNumber{" +
                    "name='" + name + '\'' +
                    ", phone='" + phone + '\'' +
                    '}';
        }
    }

    private PhoneNumber[] phoneNumbers = new PhoneNumber[10];

    public void addPhoneNumber(String name, String phone){
        for (int i = 0; i< phoneNumbers.length;i++){
            if (phoneNumbers[i]==null){
                phoneNumbers[i] = new PhoneNumber(name,phone);
                break;
            }
        }
    }

    public void printPhoneBook(){
        for (int i = 0; i < phoneNumbers.length; i++){
            if (phoneNumbers[i]==null){
                break;
            }
            System.out.println(phoneNumbers[i].toString());
        }
    }

    public void sortByName(){
        Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if (o1!=null && o2!=null){
                    return o1.name.compareToIgnoreCase(o2.name);
                }
                else {
                    return 0;
                }
            }
        });
    }

    public void sortByPhoneNumber(){
        Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if (o1!=null && o2!=null){
                    return o1.phone.compareToIgnoreCase(o2.phone);
                }
                else {
                    return 0;
                }
            }
        });
    }
}
