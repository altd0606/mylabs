package com.mainacad.modul2.labs_12.task1_2;

public class Main {
    public static void main(String[] args) {
        MyPhoneBook phoneBook = new MyPhoneBook();
        phoneBook.addPhoneNumber("vova","067987654");
        phoneBook.addPhoneNumber("name","456789876");
        phoneBook.addPhoneNumber("name2","567888888");
        phoneBook.addPhoneNumber("Sasha,","567779787");
        phoneBook.addPhoneNumber("name3","348347487");


        phoneBook.printPhoneBook();
        System.out.println();
        phoneBook.sortByName();
        phoneBook.printPhoneBook();

        phoneBook.sortByPhoneNumber();
        System.out.println();
        phoneBook.printPhoneBook();
    }
}
