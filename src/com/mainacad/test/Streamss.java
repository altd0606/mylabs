package com.mainacad.test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streamss {
    public static void main(String[] args) {
        List<Integer> i1 = Arrays.asList(1,2,3);
        List<Integer> i2 = Arrays.asList(4,5,6);
        List<Integer> i3 = Arrays.asList();
        /*Stream.of(i1,i2,i3).map(x -> x+1).flatMap(x -> x.str)*/

        ////////////
        Predicate<? super String> predicate = s -> s.startsWith("g");
        Stream<String> stream1 = Stream.generate(() -> "grow!");
        Stream<String> stream2 = Stream.generate(() -> "grow!");
        boolean b1 = stream1.anyMatch(predicate);
        boolean b2 = stream2.anyMatch(predicate);
        System.out.println(b1 + " " + b2);
        //////////////

        Stream<String> s = Stream.empty();
        Stream<String> s2 = Stream.empty();
        Map<Boolean, List<String>> p = s.collect(Collectors.partitioningBy(b -> b.startsWith("c")));
        Map<Boolean, List<String>> g = s2.collect(Collectors.groupingBy(b -> b.startsWith("c")));
        System.out.println(p + " " + g);
        System.out.println(Stream.iterate(1,x -> ++x).limit(5).map(x -> ""+ x).collect(Collectors.joining("")));

        ///////////

        Stream<String> stream = Stream.iterate("",(s1) -> s1 + "1");
        System.out.println(stream.limit(2).map(x -> x + "2").toString());

    }
}
