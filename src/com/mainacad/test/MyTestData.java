package com.mainacad.test;

import java.util.Random;

public class MyTestData {
    private byte[] data;
    public void setData(byte[] data) {
        this.data = data;
    }
    public boolean ready(){
        return data != null;
    }
}

class DataGenerator extends Thread{
    MyTestData dat;

    public DataGenerator(MyTestData dat) {
        this.dat = dat;
    }

    @Override
    public void run() {
        System.out.println("Generating Data...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        byte[] data = new byte[100];
        new Random().nextBytes(data);
        System.out.println("Ok!!!");
        synchronized (dat){
            dat.setData(data);
            dat.notifyAll();
        }
    }
}

class DataSender extends Thread{
    MyTestData data;
    String user;

    public DataSender(MyTestData data, String user) {
        this.data = data;
        this.user = user;
    }

    @Override
    public void run() {
        System.out.println("Waiting for Data #" + getId() + "...");
        synchronized (data){
            try{
                while (!data.ready()){
                    data.wait();
                    System.out.println("wait #: " + getId());
                }
            } catch (InterruptedException e) {
                return;
            }
        }
        System.out.printf("Sending data to %s\n", user);
    }
}

class Main23{
    public static void main(String[] args) {
        MyTestData data = new MyTestData();
        DataSender[] senders = {
                new DataSender(data, "user1"),
                new DataSender(data, "user2"),
                new DataSender(data, "user3")
        };
        for(DataSender sender : senders){
            sender.start();
        }
        DataGenerator pr = new DataGenerator(data);
        pr.start();
    }

}
