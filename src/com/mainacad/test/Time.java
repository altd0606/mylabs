package com.mainacad.test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;

public class Time {
    public static void main(String[] args) {
       /* LocalDateTime d = LocalDateTime.of(2015,5,10,11,22,33);
        Period p = Period.ofDays(1).ofYears(2);
        d=d.minus(p);
        DateTimeFormatter f = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        System.out.println(d.format(f));

        LocalDate date = LocalDate.parse("2018-04-30");
        date.plusDays(2);
        date.plusYears(3);
        System.out.println(date.getYear() + " " + date.getMonth() + date.getDayOfMonth());
        */

        String m1 = Duration.of(1, ChronoUnit.MINUTES).toString();
        String m2 = Duration.ofMinutes(1).toString();
        String s = Duration.of(60,ChronoUnit.SECONDS).toString();
        String d = Duration.ofDays(1).toString();
        String p = Period.ofDays(1).toString();
        System.out.println(m1.equals(s));
        LocalDate date = LocalDate.of(2018, Month.APRIL,40);
        System.out.println(date.getYear() + " " + date.getMonth() + date.getDayOfMonth());

    }
}
