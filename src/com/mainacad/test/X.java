package com.mainacad.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class X {

    public static void main(String[] args) {
        boolean flag = false;
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] parseInput = input.split(" ");
        int N = scanner.nextInt();
        int closesNum = 0;
        int[] arr = new int[parseInput.length];
        for(int i = 0; i < parseInput.length; i++){
            arr[i] = Integer.valueOf(parseInput[i]);
        }

        for(int i = 0; i <= N;i++){
            for(int j = 0; j < arr.length;j++){
                if (N-i==arr[j]){
                    closesNum = arr[j];
                    flag =true;
                    break;
                }
            }
            if (flag){break;}
        }

        for(int j = 0; j < arr.length;j++) {
            if ((N - closesNum) > (arr[j] - N) && (arr[j]-N)>0) {
                closesNum = arr[j];
                flag = true;
                break;
            }
        }


        ArrayList<Integer> rez = new ArrayList<>();

        if (N==closesNum){
            for(int i = 0; i < arr.length; i++){
                if(arr[i]==closesNum){
                    rez.add(arr[i]);
                }
            }
        }
        else {
            for(int i = 0; i < arr.length; i++){
                if(arr[i]==closesNum){
                    rez.add(arr[i]);
                }
            }

            for(int i = 0; i < arr.length; i++){
                if(arr[i]==N+N-closesNum){
                    rez.add(arr[i]);
                }
            }
        }

        Iterator<Integer> iterator = rez.iterator();
        while(iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }

    }
}
