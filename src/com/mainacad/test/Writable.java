package com.mainacad.test;

interface Writable {
    int write(String s);
}

abstract class  Write implements Writable{
    public abstract int write(String s);
}
