package com.mainacad.test;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class Test1 {

    public static void main(String[] args) {
        Long num = 12_000_000_000L;
        //BigDecimal num = BigDecimal.TWO;
        Locale loc = new Locale("uk","UA");
        NumberFormat numf = NumberFormat.getCurrencyInstance(loc);
        Currency cur = Currency.getInstance(loc);
        System.out.println(cur.getDisplayName());
        System.out.println(numf.format(num));
        System.out.println(Math.round(-10.77));
    }
}
