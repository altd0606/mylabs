package com.mainacad.test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    static ArrayList<Integer> doSomething(ArrayList<Integer> values){
        values.add(new Integer(10));
        ArrayList<Integer> tempList = new ArrayList<Integer>(values);
        tempList.add(new Integer(15));
        return tempList;
    }

/*    public static <T>List<?> asList2(T...elsements){
        ArrayList<?> temp = new ArrayList<>();
        for (T element:elsements){
            temp.add(element);
        }
    }*/

    public static void main(String[] args) {
/*        ArrayList<Integer> allValues = doSomething(new ArrayList<>());
        System.out.println(allValues);
        for(Integer i: allValues){
            System.out.println(i);
        }
        ArrayList<Object> list = new ArrayList<>();
        list.add(new String[]{"123","1234"});*/

        String hello = "hello";
        String world = "world";
        StringBuffer helloWorld = new StringBuffer(hello + world);
        List<String> list = Arrays.asList(hello,world,helloWorld.toString());
        helloWorld.append("!");
        list.remove(0);
        System.out.println(list);
    }
}
