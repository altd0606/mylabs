package com.mainacad.test;

public class MyTestThread extends Thread{
    public void run(){
        int i = 0;
        while(!isInterrupted()){
            System.out.println("Thread" + getName() + "i=" + i++);

            try{
                sleep(1000);
            }catch (InterruptedException e){
                return;
            }
        }
    }
}

class Main1{
    public static void main(String[] args) {
        MyTestThread myTestThread = new MyTestThread();
        myTestThread.start();
        try {
            Thread.sleep(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
