package com.mainacad.test;
import java.util.Scanner;
public class test2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] arr = new int[5];
        for(int i = 0; i < arr.length; i++){
            arr[i] = scanner.nextInt();
        }

        int rotationNum = scanner.nextInt();

        for(int j = 0; j < rotationNum; j++){
            int temp = 0;
            for(int i = 0; i < arr.length; i++){
                if(i==0){
                    temp = arr[i+1];
                    arr[i+1] = arr[i];
                }
                else if(i == arr.length-1){
                    arr[0] = temp;
                }
                else{
                    temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }

        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
    }
}
