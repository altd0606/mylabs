package com.mainacad.test;

public class Outer {
    private int x = 7;
    public void makeInner(){
        Inner in = new Inner();
        in.seeOuter();
    }
    private class Inner{
        private int x = 8;
        public void seeOuter(){
            System.out.println("x " + x);
        }
    }

    public static void main(String[] args) {
        Inner inner = new Outer().new Inner();
        inner.seeOuter();
    }
}
