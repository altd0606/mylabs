package com.mainacad.test;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrLen = scanner.nextInt();
        double[][] arr = new double[2][arrLen];

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < arrLen; j++){
                arr[i][j] = scanner.nextDouble();
            }
        }

        double[] rez = new double[arrLen];
        int i = 0;
        for(int j = 0; j < arrLen; j++){
            if(arr[i+1][j]==0){
                rez[j] = 0;
                break;
            }
            rez[j] = (arr[i][j]*arr[i+1][j])/(100);
        }
        int rezIndex = -1;
        double value = -1;
        boolean status = false;
        for(int j = 0; j < rez.length; j++){
            if(rez[j] > value){
                rezIndex = j;
                value = rez[j];
            }
            else if(rez[j]==value){
                rezIndex = j-1;
                int specialIndex = j-1;
                status = true;
            }
        }
        System.out.println(rezIndex+1);
    }
}
